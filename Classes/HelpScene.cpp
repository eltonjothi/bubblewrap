#include "HelpScene.h"
#include "GameWorldScene.h"
#include "Definitions.h"

USING_NS_CC;

Scene* Help::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = Help::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Help::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(255,255,255,255)) )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    isPopped = false;
    
    auto backgroundSprite = Sprite::create( "bg_front.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    this->addChild( backgroundSprite, 0);
    
    //Facebook Button
    auto fbItem = MenuItemImage::create("FB.png",
                                           "FB.png",
                                           CC_CALLBACK_1(Help::fbCallback, this));
    fbItem->setPosition(Vec2(origin.x + visibleSize.width/2 ,
                             origin.y + fbItem->getContentSize().height/2));
    auto fbMenu = Menu::create(fbItem, NULL);
    fbMenu->setPosition(Vec2::ZERO);
    this->addChild(fbMenu, CLOSE_BUTTON_LAYER);
    //FBButton Ends
    
	auto closeItem = MenuItemImage::create("quit.png",
                                           "quit.png",
                                           CC_CALLBACK_1(Help::menuCloseCallback, this));
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));
    // create menu, it's an autorelease object

    auto closeMenu = Menu::create(closeItem, NULL);
    closeMenu->setPosition(Vec2::ZERO);
    this->addChild(closeMenu, CLOSE_BUTTON_LAYER);


	auto playItem = MenuItemImage::create( "play.png", "play.png", CC_CALLBACK_1( Help::GoToGameScene, this ) );
	playItem->setPosition( Point( (playItem->getContentSize().width*1.5) + origin.x,  playItem->getContentSize().height + origin.y ) );
	auto playMenu = Menu::create( playItem, NULL );
	playMenu->setPosition( Point::ZERO );
	this->addChild( playMenu, PLAY_BUTTON_LAYER);

    auto helpPanel = Sprite::create( "panel.png" );
	helpPanel->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
	this->addChild( helpPanel, 2);

	auto moreGamesItem = MenuItemImage::create( "moregames.png", "moregames.png", CC_CALLBACK_1( Help::GoToGameScene, this ) );
	moreGamesItem->setPosition( Point( visibleSize.width-(moreGamesItem->getContentSize().width*1.5)  + origin.x,  moreGamesItem->getContentSize().height + origin.y ) );
	auto moreGamesMenu = Menu::create( moreGamesItem, NULL );
	moreGamesMenu->setPosition( Point::ZERO );
	this->addChild( moreGamesMenu, PLAY_BUTTON_LAYER);
	
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    this->setKeypadEnabled( true );
    AdmobHelper::showIAd();
    
#endif
    
    return true;
}

void Help::GoToGameScene(Ref *sender)
{
auto scene = GameWorld::createScene();
Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}

void Help::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
    MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif
    
    if(!isPopped)
        quitPopUpLayer();
    /*
     Director::getInstance()->end();
     
     #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
     exit(0);
     #endif
     */
}
void Help::onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event )
{
    if(!isPopped)
        quitPopUpLayer();
    //Director::getInstance()->end();
}
void Help::fbCallback(Ref* pSender)
{
	//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        Application::getInstance()->openURL("http://www.google.com");
	//#endif
}
void Help::quitPopUpLayer(){
    isPopped = true;
    PopupLayer* pl = PopupLayer::create("pop_panel.png");
    pl->setContentSize(CCSizeMake(400, 150));
    pl->setTitle("");
    pl->setContentText("Do you want to quit playing ?", screenHeight * SCORE_FONT_SIZE, 60, 250);
    pl->setCallbackFunc(this, callfuncN_selector(Help::buttonCallback));
    pl->addButton("pop_button.png", "pop_button.png", "No", 0);
    pl->addButton("pop_button.png", "pop_button.png", "Yes", 1);
    this->addChild(pl,20);
}
void Help::buttonCallback(cocos2d::Node *pNode){
    //CCLog("button call back. tag: %d", pNode->getTag());
    if(pNode->getTag() == 0)
    {
        isPopped = false;
    }else{
        Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        exit(0);
#endif
    }
}
