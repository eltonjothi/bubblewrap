#include "Bubble.h"
#include "Definitions.h"

USING_NS_CC;

Bubble::Bubble(  ) {}

Bubble::~Bubble() {}

Bubble* Bubble::create()
{
    Bubble* row1 = new Bubble();
    row1->initWithFile("bubble_normal.png");
    row1->autorelease();
    row1->row1Options();
    row1->addEvents();
    return row1;
    CC_SAFE_DELETE(row1);
    return NULL;
}
Bubble* Bubble::create2()
{
    Bubble* row2 = new Bubble();
    row2->initWithFile("bubble_normal.png");
    row2->autorelease();
    row2->row2Options();
    row2->addEvents();
    return row2;
    CC_SAFE_DELETE(row2);
    return NULL;
}
Bubble* Bubble::create3()
{
    Bubble* row3 = new Bubble();
    row3->initWithFile("bubble_normal.png");
    row3->autorelease();
    row3->row3Options();
    row3->addEvents();
    return row3;
    CC_SAFE_DELETE(row3);
    return NULL;
}
Bubble* Bubble::create4()
{
    Bubble* row4 = new Bubble();
    row4->initWithFile("bubble_normal.png");
    row4->autorelease();
    row4->row4Options();
    row4->addEvents();
    return row4;
    CC_SAFE_DELETE(row4);
    return NULL;
}
Bubble* Bubble::create5()
{
    Bubble* row5 = new Bubble();
    row5->initWithFile("bubble_normal.png");
    row5->autorelease();
    row5->row5Options();
    row5->addEvents();
    return row5;
    CC_SAFE_DELETE(row5);
    return NULL;
}
void Bubble::row1Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4) );

    auto bubbleAction = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4) );
    runAction( bubbleAction );
}
void Bubble::row2Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset) );
    
    auto bubbleAction2 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset) );
    runAction( bubbleAction2 );
}
void Bubble::row3Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*2) );
    
    auto bubbleAction3 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*2) );
    runAction( bubbleAction3 );
}
void Bubble::row4Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*3) );
    
    auto bubbleAction4 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*3) );
    runAction( bubbleAction4 );
}
void Bubble::row5Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*4) );
    
    auto bubbleAction5 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*4) );
    runAction( bubbleAction5 );
}
void Bubble::addEvents()
{
    listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        
        if(rect.containsPoint(p))
        {
            if(!isPopped)
            Bubble::touchEvent(touch);
            //CCLOG("containMySprite");
            return true; // to indicate that we have consumed it.
        }
        //CCLOG("dintcontainMySprite");
        return false; // we did not consume this event, pass thru.
    };
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
    //dispatchEvent(this);
}
void Bubble::touchEvent(cocos2d::Touch* touch)
{
    isPopped = true;
    setTexture("bubble_popped.png");
    GameWorld::normal();
}




