#include "LevelOverScene.h"
#include "Definitions.h"

USING_NS_CC;
unsigned int level_score;
//unsigned int speed;
unsigned int level_number;

Scene* LevelOver::createScene(unsigned int tempScore, unsigned int tempCurLvl, unsigned int type1, unsigned int type2, unsigned int type3, unsigned int type4, unsigned int type5, unsigned int type6, unsigned int type7, unsigned int type8, unsigned int type9, unsigned int type10)
{
	level_score = tempScore;
	level_number = tempCurLvl;
    auto scene = Scene::create();
    auto layer = LevelOver::create();
    scene->addChild(layer);
    return scene;
}

bool LevelOver::init()
{
    if ( !LayerColor::initWithColor(Color4B(255,255,255,255)) )
    {
        return false;
    }

    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    Director::getInstance()->getScheduler()->setTimeScale(1);
    
    isPopped = false;

    auto backgroundSprite = Sprite::create( "bg1.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    this->addChild( backgroundSprite, 0);
    
	auto closeItem = MenuItemImage::create("quit.png",
                                           "quit.png",
                                           CC_CALLBACK_1(LevelOver::menuCloseCallback, this));    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));
    // create menu, it's an autorelease object
    auto closeMenu = Menu::create(closeItem, NULL);
    closeMenu->setPosition(Vec2::ZERO);
    this->addChild(closeMenu, CLOSE_BUTTON_LAYER);

    auto scorePanel = Sprite::create( "Levelpanel.png" );
    scorePanel->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    this->addChild( scorePanel, 2);
    
    int blankspaceheight = visibleSize.height - scorePanel->getContentSize().height;
    int blankspacewidth = visibleSize.width - scorePanel->getContentSize().width;
    int offset25width = scorePanel->getContentSize().width/4;
    int offsetheight = scorePanel->getContentSize().height/15;
    
    auto LevelOverText = Label::createWithTTF("LEVEL COMPLETED", "SuperMario256.ttf", visibleSize.height * 0.05 );
    LevelOverText->setColor(Color3B::WHITE);
    //LevelOverText->enableOutline(Color4B::BLACK,2);
    LevelOverText->setPosition(Point( visibleSize.width / 2, visibleSize.height - blankspaceheight/2 - offsetheight*2));
    this->addChild(LevelOverText, SCORE_LAYER);
    
    //Rank1
    //auto star1Sprite = Sprite::create( "star.png" );
    AnimSprite* star1Sprite = AnimSprite::create("star.png",0.05,0.25);
    star1Sprite->setPosition(Point( visibleSize.width / 2 - offset25width , visibleSize.height-blankspaceheight/2- offsetheight*5));
    //star1Sprite->setScale(2);
    this->addChild( star1Sprite, SCORE_LAYER);
    
    //Rank2
    AnimSprite* star2Sprite = AnimSprite::create("star.png",0.05,0.25);
    star2Sprite->setPosition( Point( visibleSize.width / 2, visibleSize.height-blankspaceheight/2- offsetheight*4) );
    //star2Sprite->setScale(2);
    this->addChild( star2Sprite, SCORE_LAYER);
    
    //Rank3
    AnimSprite* star3Sprite = AnimSprite::create("star.png",0.05,0.25);
    star3Sprite->setPosition( Point( visibleSize.width / 2 + offset25width, visibleSize.height-blankspaceheight/2- offsetheight*5));
    //star3Sprite->setScale(2);
    this->addChild( star3Sprite, SCORE_LAYER);
    
    if(level_score>10){
    AnimSprite* star1Sprite2 = AnimSprite::create("star2.png",0.05,0.25);
    star1Sprite2->setPosition(Point( visibleSize.width / 2 - offset25width , visibleSize.height-blankspaceheight/2- offsetheight*5));
    //star1Sprite->setScale(2);
    this->addChild( star1Sprite2, SCORE_LAYER);
    }
    if(level_score>20){
    AnimSprite* star2Sprite2 = AnimSprite::create("star2.png",0.05,0.25);
    star2Sprite2->setPosition( Point( visibleSize.width / 2, visibleSize.height-blankspaceheight/2- offsetheight*4) );
    //star2Sprite->setScale(2);
    this->addChild( star2Sprite2, SCORE_LAYER);
    }
    if(level_score>30){
    AnimSprite* star3Sprite2 = AnimSprite::create("star2.png",0.05,0.25);
    star3Sprite2->setPosition( Point( visibleSize.width / 2 + offset25width, visibleSize.height-blankspaceheight/2- offsetheight*5));
    //star3Sprite->setScale(2);
    this->addChild( star3Sprite2, SCORE_LAYER);
    }
    
    
    
	__String *tempScore = __String::createWithFormat( "Score : %i", level_score );
	auto scoreLabel = Label::createWithTTF( tempScore->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_PANEL_FONT_SIZE );
    scoreLabel->setColor(Color3B::WHITE);
    //scoreLabel->enableOutline(Color4B::BLACK,2);
    scoreLabel->setPosition( Point( visibleSize.width / 2, visibleSize.height-blankspaceheight/2- offsetheight*8) );
    this->addChild( scoreLabel, SCORE_LAYER);

    AnimSprite* button1Sprite = AnimSprite::create("backbutton_normal.png",0.05,0.5);
    //button1Sprite->setPosition( Point( blankspacewidth/2, visibleSize.height-blankspaceheight/2- scorePanel->getContentSize().height));
    //button1Sprite->setScale(2);
    //this->addChild( button1Sprite, SCORE_LAYER);
    
    Sprite* button1Sprite_click = Sprite::create("backbutton_normal.png");
    //button1Sprite_click->setPosition( Point( blankspacewidth/2, visibleSize.height-blankspaceheight/2- scorePanel->getContentSize().height));
    button1Sprite_click->setScale(0.95);
    //this->addChild( button1Sprite_click, SCORE_LAYER);
    
    auto backItem = MenuItemSprite::create( button1Sprite, button1Sprite_click, CC_CALLBACK_1( LevelOver::GoToGameScene, this ) );
    backItem->setPosition( Point(blankspacewidth/2, visibleSize.height-blankspaceheight/2- scorePanel->getContentSize().height));
    auto backItemMenu = Menu::create( backItem, NULL );
    backItemMenu->setPosition( Point::ZERO );
    this->addChild( backItemMenu, PLAY_BUTTON_LAYER);
    
    AnimSprite* button2Sprite = AnimSprite::create("playbutton_normal.png",0.05,0.5);
    //button2Sprite->setPosition( Point( blankspacewidth/2 + scorePanel->getContentSize().width, visibleSize.height-blankspaceheight/2- scorePanel->getContentSize().height));
    //this->addChild( button2Sprite, SCORE_LAYER);
    Sprite* button2Sprite_click = Sprite::create("playbutton_normal.png");
    button2Sprite_click->setScale(0.95);
  
    auto playNextItem = MenuItemSprite::create( button2Sprite, button2Sprite_click, CC_CALLBACK_1( LevelOver::GoToGameScene, this ) );
    playNextItem->setPosition( Point( blankspacewidth/2 + scorePanel->getContentSize().width, visibleSize.height-blankspaceheight/2- scorePanel->getContentSize().height));
    auto playNextMenu = Menu::create( playNextItem, NULL );
    playNextMenu->setPosition( Point::ZERO );
    this->addChild( playNextMenu, PLAY_BUTTON_LAYER);
    
    __String *tempCoins = __String::createWithFormat( "Coins : %i x 10 = %i", level_number, level_number );
    auto coinsLabel = Label::createWithTTF( tempCoins->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_PANEL_FONT_SIZE );
    coinsLabel->setColor(Color3B::WHITE);
    //coinsLabel->enableOutline(Color4B::BLACK,2);
    coinsLabel->setPosition( Point( visibleSize.width / 2, visibleSize.height/2 ) );
    //this->addChild( coinsLabel, SCORE_LAYER);
    
    totalGameScore = level_score;
    __String *totalScore = __String::createWithFormat( "Total Score : %i", totalGameScore );
    auto totalScoreLabel = Label::createWithTTF( totalScore->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_PANEL_FONT_SIZE );
    totalScoreLabel->setColor(Color3B::WHITE);
    //totalScoreLabel->enableOutline(Color4B::BLACK,2);
    totalScoreLabel->setPosition( Point( visibleSize.width / 2, visibleSize.height/2 - scorePanel->getContentSize().height/6 ) );
    //this->addChild( totalScoreLabel, SCORE_LAYER);
    
    UserDefault *def = UserDefault::getInstance( );
    auto highScore = def->getIntegerForKey( &"HIGHSCORE"[level_number], 0 );
    if ( totalGameScore > highScore )
    {
        highScore = totalGameScore;
        def->setIntegerForKey( &"HIGHSCORE"[level_number], highScore );
    }
    // def->flush( );
    __String *tempHighScore = __String::createWithFormat( "Highest Score : %i", highScore );
    auto highScoreLabel = Label::createWithTTF( tempHighScore->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_PANEL_FONT_SIZE );
    highScoreLabel->setColor(Color3B::WHITE);
    //highScoreLabel->enableOutline(Color4B::BLACK,2);
    highScoreLabel->setPosition( Point( visibleSize.width / 2,  visibleSize.height-blankspaceheight/2- offsetheight*9) );
    this->addChild( highScoreLabel, 10 );


	auto moreGamesItem = MenuItemImage::create( "moregames.png", "moregames.png", CC_CALLBACK_1( LevelOver::GoToGameScene, this ) );
	moreGamesItem->setPosition( Point( visibleSize.width-(moreGamesItem->getContentSize().width*1.5)  + origin.x,  moreGamesItem->getContentSize().height + origin.y ) );
	auto moreGamesMenu = Menu::create( moreGamesItem, NULL );
	moreGamesMenu->setPosition( Point::ZERO );
	//this->addChild( moreGamesMenu, PLAY_BUTTON_LAYER);



#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    this->setKeypadEnabled( true );
#endif
    return true;
}

void LevelOver::GoToGameScene(Ref *sender)
{
    if(level_number == 1){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld2::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFadeTR::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 2){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld3::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 3){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld4::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 4){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld5::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 5){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld6::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 6){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld7::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 7){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld8::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 8){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld9::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 9){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld10::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 10){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld11::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 11){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld12::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 12){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld13::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 13){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld14::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }else if(level_number == 14){
        cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
        auto scene = GameWorld15::createScene();
        cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }
}

void LevelOver::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif
if(!isPopped)
quitPopUpLayer();
/*    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
 */
}

void LevelOver::onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event )
{
    if(!isPopped)
    quitPopUpLayer();
	//Director::getInstance()->end();
}
void LevelOver::quitPopUpLayer(){
    isPopped = true;
    PopupLayer* pl = PopupLayer::create("pop_panel.png");
    pl->setContentSize(CCSizeMake(400, 150));
    pl->setTitle("");
    pl->setContentText("Do you want to quit playing ?", 20, 60, 250);
    pl->setCallbackFunc(this, callfuncN_selector(LevelOver::buttonCallback));
    pl->addButton("pop_button.png", "pop_button.png", "No", 0);
    pl->addButton("pop_button.png", "pop_button.png", "Yes", 1);
    this->addChild(pl,20);
}
void LevelOver::buttonCallback(cocos2d::Node *pNode){
    //CCLog("button call back. tag: %d", pNode->getTag());
    if(pNode->getTag() == 0)
    {
        isPopped = false;
    }else{
        Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        exit(0);
#endif
    }
}