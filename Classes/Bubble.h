#ifndef __BUBBLE_H__
#define __BUBBLE_H__

#include "cocos2d.h"
#include "MyBodyParser.h"

#include "GameWorldScene.h"

//class Bubble
class Bubble : public cocos2d::Sprite
{
public:
    Bubble( );
    ~Bubble( );
    //cocos2d::Sprite *bubbleSprite;
    static Bubble* create();
    static Bubble* create2();
    static Bubble* create3();
    static Bubble* create4();
    static Bubble* create5();
	cocos2d::Sprite *bubble;
	cocos2d::PhysicsBody *bubbleBody;
    cocos2d::EventListenerTouchOneByOne* listener;
    
    bool isPopped = false;
    void row1Options();
    void row2Options();
    void row3Options();
    void row4Options();
    void row5Options();
    int offset;
    
    void addEvents();
    void touchEvent(cocos2d::Touch* touch);
    
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event * event);
    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
};

#endif // __BUBBLE_H__
