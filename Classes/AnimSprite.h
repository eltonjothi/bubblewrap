#ifndef __ANIMSPRITE_H__
#define __ANIMSPRITE_H__

#include "cocos2d.h"

class AnimSprite : public cocos2d::Sprite
{
public:
    AnimSprite( );
    ~AnimSprite( );
    static AnimSprite* create(const std::string& filename, float scale, float duration);
    
    bool bouncing;
    float counter;
    float oriScale;
    float animScale;
    float animDuration;
    
    void initActions();
    void update( float dt );
    //cocos2d::Sprite *bubbleSprite;
    //static Bubble* create();
    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
};

#endif
