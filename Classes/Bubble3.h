#ifndef __BUBBLE3_H__
#define __BUBBLE3_H__

#include "cocos2d.h"
#include "MyBodyParser.h"

#include "GameWorld3Scene.h"

//class Bubble
class Bubble3 : public cocos2d::Sprite
{
public:
    Bubble3( );
    ~Bubble3( );
    //cocos2d::Sprite *bubbleSprite;
    static Bubble3* create();
    static Bubble3* create2();
    static Bubble3* create3();
    static Bubble3* create4();
    static Bubble3* create5();
	cocos2d::Sprite *bubble;
	cocos2d::PhysicsBody *bubbleBody;
    
    bool isPopped = false;
    void row1Options();
    void row2Options();
    void row3Options();
    void row4Options();
    void row5Options();
    int offset;
    
    void addEvents();
    void addRedEvent();
    void addGreenEvent();
    void touchEvent(cocos2d::Touch* touch);
    void touchRedEvent(cocos2d::Touch* touch);
    void touchGreenEvent(cocos2d::Touch* touch);
    
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event * event);
    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
};

#endif // __BUBBLE3_H__
