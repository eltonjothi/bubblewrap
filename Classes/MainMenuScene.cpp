#include "MainMenuScene.h"
#include "GameWorldScene.h"
#include "GameWorld2Scene.h"
#include "GameWorld3Scene.h"
#include "GameWorld4Scene.h"
#include "GameWorld5Scene.h"
#include "GameWorld6Scene.h"
#include "GameWorld10Scene.h"
#include "GameWorld11Scene.h"
#include "GameWorld12Scene.h"
#include "Definitions.h"
#include "AnimSprite.h"

USING_NS_CC;

Scene* MainMenu::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = MainMenu::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MainMenu::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(255,255,255,255)) )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    isPopped = false;
    //Set Resolution
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;

    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(MainMenu::onTouchBegan, this);
    //listener->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
    //listener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    
    auto backgroundSprite = Sprite::create( "bg_front.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    //this->addChild( backgroundSprite, 0);

    auto titleSprite = Sprite::create( "title.png" );
    titleSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    //this->addChild( titleSprite, 1);
    
    AnimSprite* _mySprite = AnimSprite::create("FB.png",0.05,0.25);
    //Facebook Button
    auto fbItem = MenuItemSprite::create(_mySprite,
                                           _mySprite,
                                           CC_CALLBACK_1(MainMenu::fbCallback, this));
	fbItem->setPosition(Vec2(origin.x + visibleSize.width/2 ,
                                origin.y + fbItem->getContentSize().height/2));
    auto fbMenu = Menu::create(fbItem, NULL);
    fbMenu->setPosition(Vec2::ZERO);
    this->addChild(fbMenu, CLOSE_BUTTON_LAYER);
    //FBButton Ends
    
	auto closeItem = MenuItemImage::create("quit.png",
                                           "quit.png",
                                           CC_CALLBACK_1(MainMenu::menuCloseCallback, this));    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));
    // create menu, it's an autorelease object

    auto closeMenu = Menu::create(closeItem, NULL);
    closeMenu->setPosition(Vec2::ZERO);
    this->addChild(closeMenu, CLOSE_BUTTON_LAYER);


	auto playItem = MenuItemImage::create( "play.png", "play.png", CC_CALLBACK_1( MainMenu::GoToGameScene, this ) );
	playItem->setPosition( Point( visibleSize.width / 2 + origin.x,  playItem->getContentSize().height + origin.y  ) );
	auto playMenu = Menu::create( playItem, NULL );
	playMenu->setPosition( Point::ZERO );
	this->addChild( playMenu, PLAY_BUTTON_LAYER);

	auto helpItem = MenuItemImage::create( "help.png", "help.png", CC_CALLBACK_1( MainMenu::GoToHelpScene, this ) );
	helpItem->setPosition( Point( (helpItem->getContentSize().width*1.5) + origin.x,  helpItem->getContentSize().height + origin.y ) );
	auto helpMenu = Menu::create( helpItem, NULL );
	helpMenu->setPosition( Point::ZERO );
	this->addChild( helpMenu, PLAY_BUTTON_LAYER);

	auto moreGamesItem = MenuItemImage::create( "moregames.png", "moregames.png", CC_CALLBACK_1( MainMenu::GoToGameScene, this ) );
	moreGamesItem->setPosition( Point( visibleSize.width-(moreGamesItem->getContentSize().width*1.5)  + origin.x,  moreGamesItem->getContentSize().height + origin.y ) );
	auto moreGamesMenu = Menu::create( moreGamesItem, NULL );
	moreGamesMenu->setPosition( Point::ZERO );
	this->addChild( moreGamesMenu, PLAY_BUTTON_LAYER);
	
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    this->setKeypadEnabled( true );
    //AdmobHelper::showAd();
    
#endif
    
    return true;
}

void MainMenu::GoToGameScene(Ref *sender)
{
auto scene = GameWorld::createScene();
Director::getInstance( )->replaceScene( TransitionFadeTR::create( TRANSITION_TIME, scene ) );
}
void MainMenu::GoToHelpScene(Ref *sender)
{
    auto scene = GameWorld3::createScene();
    Director::getInstance( )->replaceScene( TransitionFadeTR::create( TRANSITION_TIME, scene ) );
}
void MainMenu::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif
    if(!isPopped)
        quitPopUpLayer();
}
void MainMenu::fbCallback(Ref* pSender)
{
	//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        Application::getInstance()->openURL("http://www.google.com");
	//#endif
}

void MainMenu::onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event )
{
    if(!isPopped)
        quitPopUpLayer();
}
void MainMenu::quitPopUpLayer(){
    isPopped = true;
    PopupLayer* pl = PopupLayer::create("pop_panel.png");
    pl->setContentSize(CCSizeMake(400, 150));
    pl->setTitle("");
    pl->setContentText("Do you want to quit playing ?", 20, 60, 250);
    pl->setCallbackFunc(this, callfuncN_selector(MainMenu::buttonCallback));
    pl->addButton("pop_button.png", "pop_button.png", "No", 0);
    pl->addButton("pop_button.png", "pop_button.png", "Yes", 1);
    this->addChild(pl,20);
}
void MainMenu::buttonCallback(cocos2d::Node *pNode){
    if(pNode->getTag() == 0)
    {
        isPopped = false;
    }else{
        Director::getInstance()->end();
        #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            exit(0);
        #endif
    }
}
bool MainMenu::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
    CCLOG("onTouchBegan x = %f, y = %f", touch->getLocation().x, touch->getLocation().y);
    
    return true;
}


