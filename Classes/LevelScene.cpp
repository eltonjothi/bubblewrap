//
//  LevelScene.cpp
//  BubbleWrap
//
//  Created by Elton Jothi on 07/11/14.
//
//

#include "LevelScene.h"

Scene* LevelScene::createScene()
{
    auto scene = Scene::create();
    auto layer = LevelScene::create();
    scene->addChild(layer);
    return scene;
}

bool LevelScene::init(){
    
    if ( !LayerColor::initWithColor(Color4B(255,255,255,255)) )
    {
        return false;
        
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    scrollContainer = Layer::create();
    scrollContainer->setAnchorPoint(CCPointZero);
    
    background = CCSprite::create("bg_front.png");
    //background->setScale(visibleSize.width/background->getContentSize().width);
    
    background->setPosition(Point(visibleSize.width/2 , visibleSize.height/2));
    scrollContainer->addChild(background);
    
    scrollContainer->setPosition(CCPointZero);
    Size csize = CCSizeMake(background->getScale() * background->getContentSize().width, background->getScale() * background->getContentSize().height);
    scrollContainer->setContentSize(csize);
    
    //SETUP SCROLL VIEW
    ScrollView* scrollView = ScrollView::create(visibleSize, scrollContainer);
    scrollView->setPosition(CCPointZero);
   // scrollView->setDirection(kScrollViewDirectionVertical);
    //scrollView->setDirection(kCCScrollViewDirectionVertical);
    scrollView->setContentOffset(ccp(0.f, (visibleSize.height - csize.height)), false);
    
    
    //SETUP MENU
    Sprite *btn1 = Sprite::create("pop_button.png");
    Sprite *btn2 = Sprite::create("pop_button.png");
    
    
    MenuItemSprite *button2 = MenuItemSprite::create(btn1, btn2, this, menu_selector(LevelScene::menuCallBack) );
    button2->setTag(1);
    
    // MENU
    Menu* startMenu = Menu::create( button2, NULL);
    startMenu->alignItemsHorizontallyWithPadding(visibleSize.width/13);// this is optional
    startMenu->setPosition( ccp(200, 200) ); // just a random position, you should place it where you want it
    this->addChild(startMenu,100);
    
    scrollContainer->retain();
    addChild(scrollView);
    
    return true;
    
}

void LevelScene::menuCallBack(CCObject* sender){
    
    CCMenuItem* pMenuItem = (CCMenuItem *)(sender);
    
    int tag = (int)pMenuItem->getTag();
    
    switch(tag){
            
        case 1:
            
            break;
            
        default:
            break;
    }
}

 
