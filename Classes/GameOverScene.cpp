#include "GameOverScene.h"
#include "GameWorldScene.h"
#include "Definitions.h"

USING_NS_CC;
unsigned int score;
//unsigned int speed;
unsigned int coins;

Scene* GameOver::createScene(unsigned int tempScore, unsigned int tempCoins)
{
	score = tempScore;
	coins = tempCoins;
    auto scene = Scene::create();
    auto layer = GameOver::create();
    scene->addChild(layer);
    return scene;
}

bool GameOver::init()
{
    if ( !LayerColor::initWithColor(Color4B(255,255,255,255)) )
    {
        return false;
    }

    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    isPopped = false;

    auto backgroundSprite = Sprite::create( "bg1.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    this->addChild( backgroundSprite, 0);
    
	auto closeItem = MenuItemImage::create("quit.png",
                                           "quit.png",
                                           CC_CALLBACK_1(GameOver::menuCloseCallback, this));    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));
    // create menu, it's an autorelease object
    auto closeMenu = Menu::create(closeItem, NULL);
    closeMenu->setPosition(Vec2::ZERO);
    this->addChild(closeMenu, CLOSE_BUTTON_LAYER);

    auto scorePanel = Sprite::create( "scorepanel.png" );
    scorePanel->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    this->addChild( scorePanel, 2);
    
    auto gameOverText = Label::createWithTTF("GAME OVER", "SuperMario256.ttf", visibleSize.height * 0.08 );
    gameOverText->setColor(Color3B::WHITE);
    //gameOverText->enableOutline(Color4B::BLACK,2);
    gameOverText->setPosition(Point( visibleSize.width / 2, visibleSize.height/2 + scorePanel->getContentSize().height/6 * 2  ));
    this->addChild(gameOverText, SCORE_LAYER);

	__String *tempScore = __String::createWithFormat( "Distance : %i M", score );
	auto scoreLabel = Label::createWithTTF( tempScore->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_PANEL_FONT_SIZE );
    scoreLabel->setColor(Color3B::WHITE);
    //scoreLabel->enableOutline(Color4B::BLACK,2);
    scoreLabel->setPosition( Point( visibleSize.width / 2, visibleSize.height/2 + scorePanel->getContentSize().height/6) );
    this->addChild( scoreLabel, SCORE_LAYER);
	
    __String *tempCoins = __String::createWithFormat( "Coins : %i x 10 = %i", coins, coins*10 );
    auto coinsLabel = Label::createWithTTF( tempCoins->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_PANEL_FONT_SIZE );
    coinsLabel->setColor(Color3B::WHITE);
    //coinsLabel->enableOutline(Color4B::BLACK,2);
    coinsLabel->setPosition( Point( visibleSize.width / 2, visibleSize.height/2 ) );
    this->addChild( coinsLabel, SCORE_LAYER);
    
    totalGameScore = score+coins*10;
    __String *totalScore = __String::createWithFormat( "Total Score : %i", totalGameScore );
    auto totalScoreLabel = Label::createWithTTF( totalScore->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_PANEL_FONT_SIZE );
    totalScoreLabel->setColor(Color3B::WHITE);
    //totalScoreLabel->enableOutline(Color4B::BLACK,2);
    totalScoreLabel->setPosition( Point( visibleSize.width / 2, visibleSize.height/2 - scorePanel->getContentSize().height/6 ) );
    this->addChild( totalScoreLabel, SCORE_LAYER);
    
    UserDefault *def = UserDefault::getInstance( );
    auto highScore = def->getIntegerForKey( "HIGHSCORE", 0 );
    if ( totalGameScore > highScore )
    {
        highScore = totalGameScore;
        def->setIntegerForKey( "HIGHSCORE", highScore );
    }
    // def->flush( );
    __String *tempHighScore = __String::createWithFormat( "Highest Score : %i", highScore );
    auto highScoreLabel = Label::createWithTTF( tempHighScore->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_PANEL_FONT_SIZE );
    highScoreLabel->setColor(Color3B::WHITE);
    //highScoreLabel->enableOutline(Color4B::BLACK,2);
    highScoreLabel->setPosition( Point( visibleSize.width / 2,  visibleSize.height/2 - scorePanel->getContentSize().height/6 * 2) );
    this->addChild( highScoreLabel, 10 );

	auto playAgainItem = MenuItemImage::create( "playagain.png", "playagain.png", CC_CALLBACK_1( GameOver::GoToGameScene, this ) );
	playAgainItem->setPosition( Point( (playAgainItem->getContentSize().width*1.5) + origin.x,  playAgainItem->getContentSize().height + origin.y ) );
	auto playAgainMenu = Menu::create( playAgainItem, NULL );
	playAgainMenu->setPosition( Point::ZERO );
	this->addChild( playAgainMenu, PLAY_BUTTON_LAYER);

	auto moreGamesItem = MenuItemImage::create( "moregames.png", "moregames.png", CC_CALLBACK_1( GameOver::GoToGameScene, this ) );
	moreGamesItem->setPosition( Point( visibleSize.width-(moreGamesItem->getContentSize().width*1.5)  + origin.x,  moreGamesItem->getContentSize().height + origin.y ) );
	auto moreGamesMenu = Menu::create( moreGamesItem, NULL );
	moreGamesMenu->setPosition( Point::ZERO );
	this->addChild( moreGamesMenu, PLAY_BUTTON_LAYER);



#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    this->setKeypadEnabled( true );
#endif
    return true;
}

void GameOver::GoToGameScene(Ref *sender)
{
    if(coins ==7){
auto scene = GameWorld::createScene();
Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    }
}

void GameOver::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif
if(!isPopped)
quitPopUpLayer();
/*    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
 */
}

void GameOver::onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event )
{
    if(!isPopped)
    quitPopUpLayer();
	//Director::getInstance()->end();
}
void GameOver::quitPopUpLayer(){
    isPopped = true;
    PopupLayer* pl = PopupLayer::create("pop_panel.png");
    pl->setContentSize(CCSizeMake(400, 150));
    pl->setTitle("");
    pl->setContentText("Do you want to quit playing ?", 20, 60, 250);
    pl->setCallbackFunc(this, callfuncN_selector(GameOver::buttonCallback));
    pl->addButton("pop_button.png", "pop_button.png", "No", 0);
    pl->addButton("pop_button.png", "pop_button.png", "Yes", 1);
    this->addChild(pl,20);
}
void GameOver::buttonCallback(cocos2d::Node *pNode){
    //CCLog("button call back. tag: %d", pNode->getTag());
    if(pNode->getTag() == 0)
    {
        isPopped = false;
    }else{
        Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        exit(0);
#endif
    }
}