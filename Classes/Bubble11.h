#ifndef __BUBBLE11_H__
#define __BUBBLE11_H__

#include "cocos2d.h"
#include "MyBodyParser.h"

#include "GameWorld11Scene.h"

//class Bubble
class Bubble11 : public cocos2d::Sprite
{
public:
    Bubble11( );
    ~Bubble11( );
    //cocos2d::Sprite *bubbleSprite;
    static Bubble11* create();
    static Bubble11* create2();
    static Bubble11* create3();
    static Bubble11* create4();
    static Bubble11* create5();
	cocos2d::Sprite *bubble;
	cocos2d::PhysicsBody *bubbleBody;
    
    bool isPopped = false;
    int bananaPop;
    void row1Options();
    void row2Options();
    void row3Options();
    void row4Options();
    void row5Options();
    int offset;
    
    void addEvents();
    void addFruitEvent();
    void touchEvent(cocos2d::Touch* touch);
    void touchRedEvent(cocos2d::Touch* touch);
    void touchGreenEvent(cocos2d::Touch* touch);
    void touchYellowEvent(cocos2d::Touch* touch);
    void resumeBubble(float dt);
    
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event * event);

    long preTime;
    long nextTime;
    bool isClickTwo;
    long millisecondNow();
    
    void singlet(float dt);
    void doublet(float dt);

    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
};

#endif // __Bubble11_H__
