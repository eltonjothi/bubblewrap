#include "AnimSprite.h"
#include "Definitions.h"

USING_NS_CC;

AnimSprite::AnimSprite(  ) {}

AnimSprite::~AnimSprite() {}

AnimSprite* AnimSprite::create(const std::string& filename, float scale, float duration)
{
    AnimSprite *sprite = new (std::nothrow) AnimSprite();
    if (sprite && sprite->initWithFile(filename))
    {
        sprite->counter = 0.0f;
        sprite->bouncing = true;
        //sprite->schedule(schedule_selector(AnimSprite::update),  0.25 );
        sprite->animScale = scale;
        sprite->animDuration = duration;
        sprite->initActions();
        //sprite->scheduleUpdate( );
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

void AnimSprite::initActions()
{
    /*
    auto shrinkH = ScaleTo::create(0.25,0.95,1.0);
    auto shrinkV = ScaleTo::create(0.25,1.0,0.95);
    auto swellH = ScaleTo::create(0.25,1.05,1);
    auto swellV = ScaleTo::create(0.25,1,1.05);
     */
    auto shrinkH = ScaleTo::create(animDuration, 1-animScale, 1);
    auto shrinkV = ScaleTo::create(animDuration, 1, 1-animScale);
    auto swellH = ScaleTo::create(animDuration, 1+animScale, 1);
    auto swellV = ScaleTo::create(animDuration, 1, 1+animScale);
    
    auto buttonAction = Sequence::create(shrinkH, shrinkV, swellH,swellV,  NULL);
    runAction(RepeatForever::create(buttonAction));
}
void AnimSprite::update( float dt )
{
    if (bouncing)
    {
        counter += dt;
        
        //_scaleX = ( (sin(counter*10) + 1)/2.0 * 0.1 + 2);
        //_scaleY = ( (cos(counter*10) + 1)/2.0 * 0.1 + 2);
        //CCLOG("%f,%f",_scaleX,_scaleY);
        setScale(( (sin(counter*10) + 1)/2.0 * 0.1 + 1), ( (cos(counter*10) + 1)/2.0 * 0.1 + 1));
        if (counter > M_PI*10){
            counter = 0;
        }
    }
}