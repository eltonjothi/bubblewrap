#include "GameWorldScene.h"

USING_NS_CC;


int timerCounter;
int levelScore;

Scene* GameWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics( );
    //scene->getPhysicsWorld( )->setDebugDrawMask( PhysicsWorld::DEBUGDRAW_ALL );
    
    // 'layer' is an autorelease object
    auto layer = GameWorld::create();
	//SET GRAVITY
    scene->getPhysicsWorld()->setGravity(Vect(0, 0));

	layer->SetPhysicsWorld( scene->getPhysicsWorld( ) );
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameWorld::init()
{
    if ( !LayerColor::initWithColor(Color4B(0,0,0,0)) )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    Director::getInstance()->getScheduler()->setTimeScale(2);
    
    
    auto backgroundSprite2 = Sprite::create( "pop.png" );
    backgroundSprite2->setPosition( Point(backgroundSprite2->getContentSize().width/2, backgroundSprite2->getContentSize().height/2) );
    
    isStarted=false;
    this->setTouchEnabled(false);
    //star1Sprite->setScale(2);
    
    LayerColor* _playerPopup = LayerColor::create(Color4B::BLACK);
    _playerPopup->setContentSize(CCSizeMake(visibleSize.width, backgroundSprite2->getContentSize().height));
    //_playerPopup->setPosition(Point(0, 0));
    _playerPopup->addChild(backgroundSprite2);
    auto LevelOverText = Label::createWithTTF("Pop as many bubbles possible", "SuperMario256.ttf", visibleSize.height * 0.05 );
    LevelOverText->setColor(Color3B::WHITE);
    LevelOverText->setPosition(Point( _playerPopup->getContentSize().width/2, _playerPopup->getContentSize().height/2));
    _playerPopup->addChild(LevelOverText, SCORE_LAYER);
    
    this->addChild(_playerPopup,1000);
 
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        CCLOG("CONSUME");
        return true; // to indicate that we have consumed it.
    };
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, _playerPopup);
    
    auto callback = CallFunc::create( [this]() {
       // this->removeChild(_playerPopup);
        isStarted=true;

    });
    
    
    Action* popupLayer=Sequence::create(MoveTo::create(0.5, Point(0, 0)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2+_playerPopup->getContentSize().height/8)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2-_playerPopup->getContentSize().height/6)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2)),
                                        MoveTo::create(7, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2-_playerPopup->getContentSize().height/6)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height)),
                                        RemoveSelf::create(),
                                        callback,
                                        NULL);
    _playerPopup->runAction(popupLayer);
    
    //Director::getInstance()->getScheduler()->setTimeScale(30);
    
    screenPosition = -720;
    spawnNum = 0;

    //Set Resolution
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;
    
    auto backgroundSprite = Sprite::create( "bg1.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    this->addChild( backgroundSprite, 0);
    
    auto topPanelSprite = Sprite::create( "top_panel.png" );
    topPanelSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height - topPanelSprite->getContentSize().height/2) );
    this->addChild( topPanelSprite, 1);
    
    auto closeItem = MenuItemImage::create("quit.png",
                                           "quit.png",
                                           CC_CALLBACK_1(GameWorld::menuCloseCallback, this));
    closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));
    // create menu, it's an autorelease object
    
    auto closeMenu = Menu::create(closeItem, NULL);
    closeMenu->setPosition(Vec2::ZERO);
    this->addChild(closeMenu, CLOSE_BUTTON_LAYER);
    
    isPopped = false;
    isPaused = false;
    
    auto image1 = MenuItemImage::create("sound_on_toggle.png", "sound_on_toggle.png", NULL, NULL );
    auto image2 = MenuItemImage::create("sound_off_toggle.png", "sound_off_toggle.png", NULL, NULL );
    
    auto toggle_item = MenuItemToggle::createWithCallback(CC_CALLBACK_1(GameWorld::SoundCallback, this), image1 , image2, NULL);
    toggle_item->setPosition(Vec2(origin.x + visibleSize.width - toggle_item->getContentSize().width/2 ,
                                  origin.y + visibleSize.height - toggle_item->getContentSize().height/2));
    auto soundMenu = Menu::create( toggle_item, NULL );
    soundMenu->setPosition(Vec2::ZERO);
    this->addChild(soundMenu, 100);
    
    auto play = MenuItemImage::create("play_toggle.png", "play_toggle.png", NULL, NULL );
    auto pause = MenuItemImage::create("pause_toggle.png", "pause_toggle.png", NULL, NULL );
    pp_toggle_item = MenuItemToggle::createWithCallback(CC_CALLBACK_1(GameWorld::pauseCallback, this), play , pause, NULL);
    pp_toggle_item->setPosition(Vec2(origin.x + visibleSize.width - toggle_item->getContentSize().width*2 ,
                                     origin.y + visibleSize.height - toggle_item->getContentSize().height/2));
    auto ppMenu = Menu::create(pp_toggle_item, NULL);
    ppMenu->setPosition(Vec2::ZERO);
    this->addChild(ppMenu, 100);


    //MUTE
    def = UserDefault::getInstance( );
    isMuted = def->getBoolForKey("Mute");
    if(isMuted){
        CocosDenshion::SimpleAudioEngine::getInstance( )->setBackgroundMusicVolume(0);
        CocosDenshion::SimpleAudioEngine::getInstance( )->setEffectsVolume(0);
        toggle_item->setSelectedIndex(1);
    }
    
    //SCORE
    levelScore = 0;
    timerCounter =60;
    

    auto sprite = Sprite::create("bubble_green_normal.png");
    timer = CCProgressTimer::create(sprite);
    timer->setType(ProgressTimer::Type::RADIAL);
    timer->setReverseProgress(true);
    timer->setScale(2);
    timer->setPosition(Point(visibleSize.width / 2.0f, visibleSize.height - sprite->getContentSize().height - sprite->getContentSize().height /2));
    timer->setPercentage(100);
    
    auto shrinkH = ScaleTo::create(0.25,0.95*2,1.0*2);
    auto shrinkV = ScaleTo::create(0.25,1.0*2,0.95*2);
    auto swellH = ScaleTo::create(0.25,1.05*2,1*2);
    auto swellV = ScaleTo::create(0.25,1*2,1.05*2);
    
    auto buttonAction = Sequence::create(shrinkH, shrinkV, swellH,swellV,  NULL);
    timer->runAction(RepeatForever::create(buttonAction));
    
    this->addChild(timer, 3);

    
    String *tempTimer = String::createWithFormat( "Timer - %i m", timerCounter);
    timerLabel = Label::createWithTTF( tempTimer->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_FONT_SIZE );
    timerLabel->setColor( Color3B::GRAY );
    timerLabel->enableOutline(Color4B::BLACK,2);
    timerLabel->setPosition( Point(  screenWidth-screenWidth/6, screenHeight - screenHeight/10) );
    this->addChild( timerLabel, 50);
    
    String *tempScore = String::createWithFormat( "Score - %i", levelScore );
    scoreLabel = Label::createWithTTF( tempScore->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_FONT_SIZE );
    scoreLabel->setColor( Color3B::ORANGE );
    scoreLabel->enableOutline(Color4B::BLACK,2);
    scoreLabel->setPosition( Point(  screenWidth/6, screenHeight - screenHeight/10) );
    this->addChild( scoreLabel, 22);
    
    
    this->schedule(schedule_selector(GameWorld::SpawnEnemy),  BUBBLE_SPEED ); //3
    this->schedule(schedule_selector(GameWorld::UpdateTimer),1);
    this->scheduleOnce(schedule_selector(GameWorld::normalSpeed), 10 );
    this->scheduleUpdate( );
    return true;
}
void GameWorld::normalSpeed(float dt)
{
    Director::getInstance()->getScheduler()->setTimeScale(1);
}

void GameWorld::UpdateTimer(float dt)
{
    if(isStarted)
    timerCounter--;
    
    if (timerCounter < 0)
    {
        unschedule(schedule_selector(GameWorld::SpawnEnemy));
        unschedule(schedule_selector(GameWorld::UpdateTimer));
        this->scheduleOnce( schedule_selector( GameWorld::NextLevel ), 0 );

    }
    timer->setPercentage(timerCounter*1.66);
}

void  GameWorld::normal()
{
    levelScore++;
}

void GameWorld::SpawnEnemy(float dt)
{
    Bubble* _mySprite = Bubble::create();
    this->addChild(_mySprite,100);
    Bubble* _mySprite2 = Bubble::create2();
    this->addChild(_mySprite2,100);
    Bubble* _mySprite3 = Bubble::create3();
    this->addChild(_mySprite3,100);
    Bubble* _mySprite4 = Bubble::create4();
    this->addChild(_mySprite4,100);
    Bubble* _mySprite5 = Bubble::create5();
    this->addChild(_mySprite5,100);
}
void GameWorld::NextLevel( float dt )
{
    cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
    auto scene = LevelOver::createScene(levelScore,1,2,3,4,0,0,0,0,0,0,0);
    cocos2d::Director::getInstance( )->replaceScene( TransitionFadeTR::create( TRANSITION_TIME, scene ) );
}


void GameWorld::update( float dt )
{
    __String *tempTimer = __String::createWithFormat( "Time - %i m", timerCounter );
    timerLabel->setString( tempTimer->getCString( ) );
    
    __String *tempScore = __String::createWithFormat( "Score - %i ", levelScore );
    scoreLabel->setString( tempScore->getCString( ) );
}

void GameWorld::pauseNodeAndDescendants(Node *pNode)
{
    pNode->pause();
    for(const auto &child : pNode->getChildren())
    {
        this->pauseNodeAndDescendants(child);
    }
}

void GameWorld::resumeNodeAndDescendants(Node *pNode)
{
    pNode->resume();
    for(const auto &child : pNode->getChildren())
    {
        this->resumeNodeAndDescendants(child);
    }
}

void GameWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
    MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif
    if(!isPopped){
    quitPopUpLayer();
    this->pause();
    pauseNodeAndDescendants(this);
    isPaused=true;
    }
}

void GameWorld::pauseCallback(Ref* pSender)
{
    
    auto toggleItem = dynamic_cast<MenuItemToggle*>(pSender);
    if (toggleItem->getSelectedIndex() == 1 ) {
        this->pause();
        pauseNodeAndDescendants(this);
        isPaused=true;
    }else if ( toggleItem->getSelectedIndex() == 0 ) {
        this->resume();
        resumeNodeAndDescendants(this);
        isPaused=false;
    }
}

void GameWorld::SoundCallback(Ref* pSender2)
{
    auto toggleItem = dynamic_cast<MenuItemToggle*>(pSender2);
    if (toggleItem->getSelectedIndex() == 1 ) {
        CocosDenshion::SimpleAudioEngine::getInstance( )->setBackgroundMusicVolume(0);
        CocosDenshion::SimpleAudioEngine::getInstance( )->setEffectsVolume(0);
        isMuted=true;
        //CCLOG("Muted");
    }
    
    else if ( toggleItem->getSelectedIndex() == 0 ) {
        CocosDenshion::SimpleAudioEngine::getInstance( )->setBackgroundMusicVolume(1);
        CocosDenshion::SimpleAudioEngine::getInstance( )->setEffectsVolume(1);
        isMuted=false;
        //CCLOG("Unmuted");
    }
    def->setBoolForKey("Mute", isMuted);
    def->flush( );
}

void GameWorld::onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event )
{
    if(!isPopped)
    {
        quitPopUpLayer();
        this->pause();
        pauseNodeAndDescendants(this);
        isPaused=true;
    }
}
void GameWorld::quitPopUpLayer(){
    isPopped = true;
    PopupLayer* pl = PopupLayer::create("pop_panel.png");
    pl->setContentSize(CCSizeMake(400, 150));
    pl->setTitle("");
    pl->setContentText("Do you want to quit playing ?", 20, 60, 250);
    pl->setCallbackFunc(this, callfuncN_selector(GameWorld::buttonCallback));
    pl->addButton("pop_button.png", "pop_button.png", "No", 0);
    pl->addButton("pop_button.png", "pop_button.png", "Yes", 1);
    this->addChild(pl,10000);
}
void GameWorld::buttonCallback(cocos2d::Node *pNode){
    if(pNode->getTag() == 0)
    {
        isPopped = false;
        this->resume();
        resumeNodeAndDescendants(this);
    }else{
        Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        exit(0);
#endif
    }
}
