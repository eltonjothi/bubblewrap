//
//  PopupLayer.cpp
//  TestCpp
//
//  Created by leafsoar on 7/29/13.
//
//

#include "PopupLayer.h"

PopupLayer::PopupLayer():
m__pMenu(NULL)
, m_contentPadding(0)
, m_contentPaddingTop(0)
, m_callbackListener(NULL)
, m_callback(NULL)
, m__sfBackGround(NULL)
, m__s9BackGround(NULL)
, m__ltContentText(NULL)
, m__ltTitle(NULL)
{
    
}

PopupLayer::~PopupLayer(){
    CC_SAFE_RELEASE(m__pMenu);
    CC_SAFE_RELEASE(m__sfBackGround);
    CC_SAFE_RELEASE(m__ltContentText);
    CC_SAFE_RELEASE(m__ltTitle);
    CC_SAFE_RELEASE(m__s9BackGround);
}

bool PopupLayer::init(){
    bool bRef = false;
    do{
        CC_BREAK_IF(!Layer::init());
        this->setContentSize(CCSizeZero);
        
        // 初始化需要的 Menu
        auto menu = Menu::create();
        menu->setPosition(Vec2::ZERO);
        setMenuButton(menu);
        
        //setTouchEnabled(true);
        
        bRef = true;
    }while(0);
    return bRef;
}
/*
void PopupLayer::registerWithTouchDispatcher(){
    // 这里的触摸优先级设置为 -128 这保证了，屏蔽下方的触摸
    auto touchListener = EventListenerTouchOneByOne::create( );
    touchListener->setSwallowTouches( true );
    touchListener->onTouchBegan = CC_CALLBACK_2( PopupLayer::onTouchBegan, this );
    Director::getInstance( )->getEventDispatcher( )->addEventListenerWithSceneGraphPriority( touchListener, this );

}*/

bool PopupLayer::TouchBegan(cocos2d::Touch *pTouch, cocos2d::Event *pEvent){
    //
    log("PopupLayer touch");
    return true;
}

PopupLayer* PopupLayer::create(const char *backgroundImage){
    PopupLayer* ml = PopupLayer::create();
    ml->setSpriteBackGround(CCSprite::create(backgroundImage));
    ml->setSprite9BackGround(Scale9Sprite::create(backgroundImage));
    return ml;
}

void PopupLayer::setTitle(const char *title, int fontsize){
    auto ltfTitle = LabelTTF::create(title, "", fontsize);
    setLabelTitle(ltfTitle);
}

void PopupLayer::setContentText(const char *text, int fontsize, int padding, int paddingTop){
    auto ltf = LabelTTF::create(text, "", fontsize);
    setLabelContentText(ltf);
    m_contentPadding = padding;
    m_contentPaddingTop = paddingTop;
}

void PopupLayer::setCallbackFunc(cocos2d::Object *target, SEL_CallFuncN callfun){
    m_callbackListener = target;
    m_callback = callfun;    
}


bool PopupLayer::addButton(const char *normalImage, const char *selectedImage, const char *title, int tag){
    Size winSize = Director::getInstance()->getVisibleSize();
    Point pCenter = Point(winSize.width / 2, winSize.height / 2);
    
    // 创建图片菜单按钮
    auto menuImage = MenuItemImage::create(normalImage, selectedImage, this, menu_selector(PopupLayer::buttonCallback));
    menuImage->setTag(tag);
    menuImage->setPosition(pCenter);
    
    // 添加文字说明并设置位置
    Size imenu = menuImage->getContentSize();
    auto ttf = CCLabelTTF::create(title, "", 20);
    ttf->setColor(Color3B::WHITE );
    ttf->setPosition(Point(imenu.width / 2, imenu.height / 2));
    menuImage->addChild(ttf);
    
    getMenuButton()->addChild(menuImage);
    return true;
}

void PopupLayer::buttonCallback(cocos2d::Object *pSender){
    Node* node = dynamic_cast<Node*>(pSender);
    log("touch tag: %d", node->getTag());
    if (m_callback && m_callbackListener){
        (m_callbackListener->*m_callback)(node);
    }
    this->removeFromParent();
}

void PopupLayer::onEnter(){
    CCLayer::onEnter();
    
    Size winSize = Director::getInstance()->getVisibleSize();
    Point pCenter = Point(winSize.width / 2, winSize.height / 2);
    
    Size contentSize;
    // 设定好参数，在运行时加载
    if (getContentSize().equals(CCSizeZero)) {
        getSpriteBackGround()->setPosition(Point(winSize.width / 2, winSize.height / 2));
        this->addChild(getSpriteBackGround(), 0, 0);
        contentSize = getSpriteBackGround()->getTexture()->getContentSize();
    } else {
        Scale9Sprite *background = getSprite9BackGround();
        background->setContentSize(getContentSize());
        background->setPosition(Point(winSize.width / 2, winSize.height / 2));
        this->addChild(background, 0, 0);
        contentSize = getContentSize();
    }
    
    
    // 添加按钮，并设置其位置
    this->addChild(getMenuButton());
    float btnWidth = contentSize.width / (getMenuButton()->getChildrenCount() + 1);
    
    int i = 0;
    for(const auto &pObj : getMenuButton()->getChildren()){
        Node* node = dynamic_cast<Node*>(pObj);
        node->setPosition(Point( winSize.width / 2 - contentSize.width / 2 + btnWidth * (i + 1), winSize.height / 2 - contentSize.height / 3));
        i++;
    }
    
    
    // 显示对话框标题
    if (getLabelTitle()){
        getLabelTitle()->setPosition(pCenter + Point(0, contentSize.height / 2 - 35.0f));
        this->addChild(getLabelTitle());
    }
    
    // 显示文本内容
    if (getLabelContentText()){
        auto ltf = getLabelContentText();
        ltf->setPosition(Point(winSize.width / 2, winSize.height / 2));
        ltf->setDimensions(CCSizeMake(contentSize.width - m_contentPadding * 2, contentSize.height - m_contentPaddingTop));
        ltf->setHorizontalAlignment(kCCTextAlignmentLeft);
        this->addChild(ltf);
    }

    // 弹出效果
    Action* popupLayer = Sequence::create(ScaleTo::create(0.0, 0.0),
                                              ScaleTo::create(0.06, 1.05),
                                              ScaleTo::create(0.08, 0.95),
                                              ScaleTo::create(0.08, 1.0), NULL);
    this->runAction(popupLayer);

}

void PopupLayer::onExit(){
    
    log("popup on exit.");
    CCLayer::onExit();
}
