#ifndef __BUBBLE15_H__
#define __BUBBLE15_H__

#include "cocos2d.h"
#include "MyBodyParser.h"

#include "GameWorld15Scene.h"

//class Bubble
class Bubble15 : public cocos2d::Sprite
{
public:
    Bubble15( );
    ~Bubble15( );
    //cocos2d::Sprite *bubbleSprite;
    static Bubble15* create();
    static Bubble15* create2();
    static Bubble15* create3();
    static Bubble15* create4();
    static Bubble15* create5();
	cocos2d::Sprite *bubble;
	cocos2d::PhysicsBody *bubbleBody;
    
    bool isPopped = false;
    void row1Options();
    void row2Options();
    void row3Options();
    void row4Options();
    void row5Options();
    int offset;
    
    void addEvents();
    void addRedEvent();
    void addYellowEvent();
    void addGreenEvent();
    void touchEvent(cocos2d::Touch* touch);
    void touchRedEvent(cocos2d::Touch* touch);
    void touchGreenEvent(cocos2d::Touch* touch);
    void touchYellowEvent(cocos2d::Touch* touch);
    void resumeBubble(float dt);
    
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event * event);
    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
};

#endif // __Bubble15_H__
