#include "Bubble3.h"
#include "Definitions.h"

USING_NS_CC;

Bubble3::Bubble3(  ) {}

Bubble3::~Bubble3() {}

unsigned int greennumber=0;

Bubble3* Bubble3::create()
{
    Bubble3* row1 = new Bubble3();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.99)
    {
        row1->initWithFile("bubble_normal.png");
        row1->autorelease();
        row1->row1Options();
        row1->addEvents();
    }else{
        greennumber++;
        if(greennumber<3){
        row1->initWithFile("bubble_green_normal.png");
        row1->autorelease();
        row1->row1Options();
        row1->addGreenEvent();
        }else{
            row1->initWithFile("bubble_normal.png");
            row1->autorelease();
            row1->row1Options();
            row1->addEvents();
        }
    }
    return row1;
    CC_SAFE_DELETE(row1);
    return NULL;
}
Bubble3* Bubble3::create2()
{
    Bubble3* row2 = new Bubble3();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.99 )
    {
        row2->initWithFile("bubble_normal.png");
        row2->autorelease();
        row2->row2Options();
        row2->addEvents();
    }else{
        greennumber++;
        if(greennumber<3){
        row2->initWithFile("bubble_green_normal.png");
        row2->autorelease();
        row2->row2Options();
        row2->addGreenEvent();
        }else{
            row2->initWithFile("bubble_normal.png");
            row2->autorelease();
            row2->row2Options();
            row2->addEvents();
        }
    }
    return row2;
    CC_SAFE_DELETE(row2);
    return NULL;
}
Bubble3* Bubble3::create3()
{
    Bubble3* row3 = new Bubble3();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.99 )
    {
        row3->initWithFile("bubble_normal.png");
        row3->autorelease();
        row3->row3Options();
        row3->addEvents();
    }else{
        greennumber++;
        if(greennumber<3){
        row3->initWithFile("bubble_green_normal.png");
        row3->autorelease();
        row3->row3Options();
        row3->addGreenEvent();
        }else{
            row3->initWithFile("bubble_normal.png");
            row3->autorelease();
            row3->row3Options();
            row3->addEvents();
        }
    }
    return row3;
    CC_SAFE_DELETE(row3);
    return NULL;
}
Bubble3* Bubble3::create4()
{
    Bubble3* row4 = new Bubble3();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.99 )
    {
        row4->initWithFile("bubble_normal.png");
        row4->autorelease();
        row4->row4Options();
        row4->addEvents();
    }else{
        greennumber++;
        if(greennumber<3){
        row4->initWithFile("bubble_green_normal.png");
        row4->autorelease();
        row4->row4Options();
        row4->addGreenEvent();
        }else{
            row4->initWithFile("bubble_normal.png");
            row4->autorelease();
            row4->row4Options();
            row4->addEvents();
        }
    }
    return row4;
    CC_SAFE_DELETE(row4);
    return NULL;
}
Bubble3* Bubble3::create5()
{
    Bubble3* row5 = new Bubble3();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.99 )
    {
        row5->initWithFile("bubble_normal.png");
        row5->autorelease();
        row5->row5Options();
        row5->addEvents();
    }else{
        greennumber++;
        if(greennumber<3){
        row5->initWithFile("bubble_green_normal.png");
        row5->autorelease();
        row5->row5Options();
        row5->addGreenEvent();
        }else{
            row5->initWithFile("bubble_normal.png");
            row5->autorelease();
            row5->row5Options();
            row5->addEvents();
        }
    }
    return row5;
    CC_SAFE_DELETE(row5);
    return NULL;
}
void Bubble3::row1Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4) );
    
    auto bubbleAction = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4) );
    runAction( bubbleAction );
}
void Bubble3::row2Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset) );
    
    auto bubbleAction2 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset) );
    runAction( bubbleAction2 );
}
void Bubble3::row3Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*2) );
    
    auto bubbleAction3 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*2) );
    runAction( bubbleAction3 );
}
void Bubble3::row4Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*3) );
    
    auto bubbleAction4 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*3) );
    runAction( bubbleAction4 );
}
void Bubble3::row5Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*4) );
    
    auto bubbleAction5 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*4) );
    runAction( bubbleAction5 );
}
void Bubble3::addEvents()
{
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        
        if(rect.containsPoint(p))
        {
            if(!isPopped)
                Bubble3::touchEvent(touch);
            //CCLOG("containMySprite");
            return true; // to indicate that we have consumed it.
        }
        //CCLOG("dintcontainMySprite");
        return false; // we did not consume this event, pass thru.
    };
    //listener->onTouchMoved = CC_CALLBACK_2(Bubble::onTouchMoved, this);
    
    listener->onTouchEnded = [=](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        if(!isPopped)
            Bubble3::touchEvent(touch);
    };
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}
void Bubble3::addGreenEvent()
{
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        
        if(rect.containsPoint(p))
        {
            if(!isPopped)
                Bubble3::touchGreenEvent(touch);
            //CCLOG("containMySprite");
            return true; // to indicate that we have consumed it.
        }
        //CCLOG("dintcontainMySprite");
        return false; // we did not consume this event, pass thru.
    };
    
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}
void Bubble3::touchEvent(cocos2d::Touch* touch)
{
    isPopped = true;
    setTexture("bubble_popped.png");
    GameWorld3::normal();
    //CCLOG("touched MySprite");
}

void Bubble3::touchGreenEvent(cocos2d::Touch* touch)
{
    isPopped = true;
    setTexture("bubble_popped.png");
    GameWorld3::green();
    cocos2d::Sprite* rowx =  cocos2d::Sprite::create( "bubble_strawberry.png" );
    auto emitter1 = ParticleSystemQuad::create("LavaFlow.plist");
    emitter1->setStartColor(Color4F(0.83,0.01,0.184,1));
    //emitter1->setStartColor(Color4F::MAGENTA);
    emitter1->setPosition(Point( rowx->getContentSize().width/2, rowx->getContentSize().height/2));
    emitter1->retain();
    addChild(emitter1, 100);
    //CCLOG("touched red");
}





