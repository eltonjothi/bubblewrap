#ifndef __GAMEWORLD7_SCENE_H__
#define __GAMEWORLD7_SCENE_H__


#include "cocos2d.h"

#include "Definitions.h"
#include "Bubble7.h"

#include "GameOverScene.h"
#include "LevelOverScene.h"
#include "GameWorld8Scene.h"

#include "Parallax/CCParallaxScrollNode.h"
#include "Parallax/CCParallaxScrollOffset.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AdmobHelper.h"
#endif

class GameWorld7 : public cocos2d::LayerColor
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    cocos2d::ProgressTo* ProgressBar;
    
    float screenWidth;
    float screenHeight;
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    unsigned int life;
    unsigned int coins;
    //unsigned int speed;
    unsigned int counter;
    unsigned int spawnNum;
    unsigned int coinSpawnNum;
    cocos2d::Label *scoreLabel;
    cocos2d::Label *timerLabel;
    cocos2d::Sprite *hero2;
    
    void normalSpeed( float dt );
	void update( float dt );
    void SpawnEnemy(float dt);
    void yellowResume(float dt);
    
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    
    float screenPosition;
    float screenSpeed;
    
    MenuItemToggle* pp_toggle_item;
    void onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event );
    bool isPaused;
    bool isPopped;
    bool isMuted;
    bool isStarted;
    
    UserDefault *def;
    void UpdateTimer(float dt);
    void pauseNodeAndDescendants(Node *pNode);
    void resumeNodeAndDescendants(Node *pNode);
    void pauseCallback(cocos2d::Ref* pSender);
    void SoundCallback(cocos2d::Ref* pSender2);
    void quitPopUpLayer();
    void buttonCallback(cocos2d::Node *pNode);
    
    void NextLevel( float dt );
    
    static void normal();
    static void orange(float Posx, float Posy);
    static void red();
    static void green();
    static void yellow();
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameWorld7);
private:
	cocos2d::PhysicsWorld *sceneWorld;
    void SetPhysicsWorld( cocos2d::PhysicsWorld *world ) { sceneWorld = world; };
	bool onContactBegin (cocos2d::PhysicsContact &contact);
};

#endif // __HELLOWORLD_SCENE_H__
