#include "Bubble11.h"
#include "Definitions.h"

USING_NS_CC;

Bubble11::Bubble11(  ) {
    
    preTime = 0.0f;
    nextTime = 0.0f;
    isClickTwo  = false;
    bananaPop = 0;
}

Bubble11::~Bubble11() {}

long Bubble11::millisecondNow()
{
    struct timeval now;
    gettimeofday(&now, NULL);
    return (now.tv_sec * 1000 + now.tv_usec / 1000);
}

Bubble11* Bubble11::create()
{
    Bubble11* row1 = new Bubble11();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.90 )
    {
        row1->initWithFile("bubble_normal.png");
        row1->setName("normal");
        row1->autorelease();
        row1->row1Options();
        row1->addEvents();
    }else if (randomBubble >= 0.90 && randomBubble < 0.95){
        row1->initWithFile("bubble_apple.png");
        row1->setName("apple");
        row1->autorelease();
        row1->row1Options();
        row1->addFruitEvent();
    }else if ( randomBubble >= 0.95){
        row1->initWithFile("bubble_banana.png");
        row1->setName("banana");
        row1->autorelease();
        row1->row1Options();
        row1->addFruitEvent();
    }
    return row1;
    CC_SAFE_DELETE(row1);
    return NULL;
}
Bubble11* Bubble11::create2()
{
    Bubble11* row2 = new Bubble11();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.90 )
    {
        row2->initWithFile("bubble_normal.png");
        row2->setName("normal");
        row2->autorelease();
        row2->row2Options();
        row2->addEvents();
    }else if (randomBubble >= 0.90 && randomBubble < 0.95){
        row2->initWithFile("bubble_apple.png");
        row2->setName("apple");
        row2->autorelease();
        row2->row2Options();
        row2->addFruitEvent();
    }else if ( randomBubble >= 0.95){
        row2->initWithFile("bubble_banana.png");
        row2->setName("banana");
        row2->autorelease();
        row2->row2Options();
        row2->addFruitEvent();
    }
    return row2;
    CC_SAFE_DELETE(row2);
    return NULL;
}
Bubble11* Bubble11::create3()
{
    Bubble11* row3 = new Bubble11();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.90 )
    {
        row3->initWithFile("bubble_normal.png");
        row3->setName("normal");
        row3->autorelease();
        row3->row3Options();
        row3->addEvents();
    }else if (randomBubble >= 0.90 && randomBubble < 0.95){
        row3->initWithFile("bubble_apple.png");
        row3->setName("apple");
        row3->autorelease();
        row3->row3Options();
        row3->addFruitEvent();
    }else if ( randomBubble >= 0.95){
        row3->initWithFile("bubble_banana.png");
        row3->setName("banana");
        row3->autorelease();
        row3->row3Options();
        row3->addFruitEvent();
    }
    return row3;
    CC_SAFE_DELETE(row3);
    return NULL;
}
Bubble11* Bubble11::create4()
{
    Bubble11* row4 = new Bubble11();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.90 )
    {
        row4->initWithFile("bubble_normal.png");
        row4->setName("normal");
        row4->autorelease();
        row4->row4Options();
        row4->addEvents();
    }else if (randomBubble >= 0.90 && randomBubble < 0.95){
        row4->initWithFile("bubble_apple.png");
        row4->setName("apple");
        row4->autorelease();
        row4->row4Options();
        row4->addFruitEvent();
    }else if ( randomBubble >= 0.95){
        row4->initWithFile("bubble_banana.png");
        row4->setName("banana");
        row4->autorelease();
        row4->row4Options();
        row4->addFruitEvent();
    }
    return row4;
    CC_SAFE_DELETE(row4);
    return NULL;
}
Bubble11* Bubble11::create5()
{
    Bubble11* row5 = new Bubble11();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.90 )
    {
        row5->initWithFile("bubble_normal.png");
        row5->setName("normal");
        row5->autorelease();
        row5->row5Options();
        row5->addEvents();
    }else if (randomBubble >= 0.90 && randomBubble < 0.95){
        row5->initWithFile("bubble_apple.png");
        row5->setName("apple");
        row5->autorelease();
        row5->row5Options();
        row5->addFruitEvent();
    }else if ( randomBubble >= 0.95){
        row5->initWithFile("bubble_banana.png");
        row5->setName("banana");
        row5->autorelease();
        row5->row5Options();
        row5->addFruitEvent();
    }
    return row5;
    CC_SAFE_DELETE(row5);
    return NULL;
}
void Bubble11::row1Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4) );
    
    auto bubbleAction = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4) );
    runAction( bubbleAction );
}
void Bubble11::row2Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset) );
    
    auto bubbleAction2 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset) );
    runAction( bubbleAction2 );
}
void Bubble11::row3Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*2) );
    
    auto bubbleAction3 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*2) );
    runAction( bubbleAction3 );
}
void Bubble11::row4Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*3) );
    
    auto bubbleAction4 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*3) );
    runAction( bubbleAction4 );
}
void Bubble11::row5Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*4) );
    
    auto bubbleAction5 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*4) );
    runAction( bubbleAction5 );
}
void Bubble11::addEvents()
{
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        
        if(rect.containsPoint(p))
        {
            if(!isPopped)
                Bubble11::touchEvent(touch);
            return true; // to indicate that we have consumed it.
        }
        //CCLOG("dintcontainMySprite");
        return false; // we did not consume this event, pass thru.
    };
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}
void Bubble11::addFruitEvent()
{
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        if(rect.containsPoint(p))
        {
            if(!isPopped){
                isClickTwo = false;
                nextTime = millisecondNow();
                if ((nextTime - preTime < 300) && (nextTime - preTime > 50)) {
                    scheduleOnce( schedule_selector(Bubble11::doublet), 0 );
                    isClickTwo = true;
                }
                scheduleOnce(schedule_selector(Bubble11::singlet), 0.25f);
                preTime = millisecondNow();
            }
            //Bubble6::touchRedEvent(touch);
            return true; // to indicate that we have consumed it.
        }
        return false; // we did not consume this event, pass thru.
    };
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}
void Bubble11::touchEvent(cocos2d::Touch* touch)
{
    isPopped = true;
    //removeChildByTag(int tag);
    setTexture("bubble_popped.png");
    
    if(getName() == "normal"){
        CCLOG("normality");
    }
    GameWorld11::normal();
}

void Bubble11::doublet(float dt)
{
    //this is Double click
    if(getName() == "banana"){
        if(bananaPop >= 1){
        isPopped = true;
        setTexture("bubble_popped.png");
        cocos2d::Sprite* rowx =  cocos2d::Sprite::create( "bubble_strawberry.png" );
        auto emitter1 = ParticleSystemQuad::create("LavaFlow.plist");
        emitter1->setStartColor(Color4F(0.83,0.01,0.184,1));
        //emitter1->setStartColor(Color4F::MAGENTA);
        emitter1->setPosition(Point( rowx->getContentSize().width/2, rowx->getContentSize().height/2));
        emitter1->retain();
        addChild(emitter1, 100);
        }else{
            isPopped = true;
            int fruit=2;
            setTexture("bubble_popped.png");
            GameWorld11::fruit(getPositionX(),getPositionY(),fruit);
        }
    }else{
    isPopped = true;
    setTexture("bubble_popped.png");
    cocos2d::Sprite* rowx =  cocos2d::Sprite::create( "bubble_strawberry.png" );
    auto emitter1 = ParticleSystemQuad::create("LavaFlow.plist");
    emitter1->setStartColor(Color4F(0.83,0.01,0.184,1));
    //emitter1->setStartColor(Color4F::MAGENTA);
    emitter1->setPosition(Point( rowx->getContentSize().width/2, rowx->getContentSize().height/2));
    emitter1->retain();
    addChild(emitter1, 100);
    }
}
void Bubble11::singlet(float dt)
{
    //this is Single click
    if (! isClickTwo) {
        bananaPop++;
        int fruit=0;
        if(getName() == "apple"){
            fruit=1;
            isPopped = true;
            setTexture("bubble_popped.png");
            GameWorld11::fruit(getPositionX(),getPositionY(),fruit);
        }
        if(getName() == "banana"){
            fruit=2;
            if(bananaPop == 2){
            isPopped = true;
            setTexture("bubble_popped.png");
            GameWorld11::fruit(getPositionX(),getPositionY(),fruit);
            }
        }
        
    }
}


