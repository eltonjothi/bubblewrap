#include "Bubble8.h"
#include "Definitions.h"

USING_NS_CC;

Bubble8::Bubble8(  ) {
    
    preTime = 0.0f;
    nextTime = 0.0f;
    isClickTwo  = false;

}

Bubble8::~Bubble8() {}

long Bubble8::millisecondNow()
{
    struct timeval now;
    gettimeofday(&now, NULL);
    return (now.tv_sec * 1000 + now.tv_usec / 1000);
}

Bubble8* Bubble8::create()
{
    Bubble8* row1 = new Bubble8();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.80 )
    {
        row1->initWithFile("bubble_normal.png");
        row1->autorelease();
        row1->row1Options();
        row1->addEvents();
    }else{
        row1->initWithFile("bubble_grape.png");
        row1->autorelease();
        row1->row1Options();
        row1->addGrapeEvent();
    }
    return row1;
    CC_SAFE_DELETE(row1);
    return NULL;
}
Bubble8* Bubble8::create2()
{
    Bubble8* row2 = new Bubble8();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.80 )
    {
        row2->initWithFile("bubble_normal.png");
        row2->autorelease();
        row2->row2Options();
        row2->addEvents();
    }else{
        row2->initWithFile("bubble_grape.png");
        row2->autorelease();
        row2->row2Options();
        row2->addGrapeEvent();
    }
    return row2;
    CC_SAFE_DELETE(row2);
    return NULL;
}
Bubble8* Bubble8::create3()
{
    Bubble8* row3 = new Bubble8();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.80 )
    {
        row3->initWithFile("bubble_normal.png");
        row3->autorelease();
        row3->row3Options();
        row3->addEvents();
    }else{
        row3->initWithFile("bubble_grape.png");
        row3->autorelease();
        row3->row3Options();
        row3->addGrapeEvent();
    }
    return row3;
    CC_SAFE_DELETE(row3);
    return NULL;
}
Bubble8* Bubble8::create4()
{
    Bubble8* row4 = new Bubble8();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.80 )
    {
        row4->initWithFile("bubble_normal.png");
        row4->autorelease();
        row4->row4Options();
        row4->addEvents();
    }else{
        row4->initWithFile("bubble_grape.png");
        row4->autorelease();
        row4->row4Options();
        row4->addGrapeEvent();
    }
    return row4;
    CC_SAFE_DELETE(row4);
    return NULL;
}
Bubble8* Bubble8::create5()
{
    Bubble8* row5 = new Bubble8();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.80 )
    {
        row5->initWithFile("bubble_normal.png");
        row5->autorelease();
        row5->row5Options();
        row5->addEvents();
    }else{
        row5->initWithFile("bubble_grape.png");
        row5->autorelease();
        row5->row5Options();
        row5->addGrapeEvent();
    }
    return row5;
    CC_SAFE_DELETE(row5);
    return NULL;
}
void Bubble8::row1Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4) );
    
    auto bubbleAction = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4) );
    runAction( bubbleAction );
}
void Bubble8::row2Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset) );
    
    auto bubbleAction2 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset) );
    runAction( bubbleAction2 );
}
void Bubble8::row3Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*2) );
    
    auto bubbleAction3 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*2) );
    runAction( bubbleAction3 );
}
void Bubble8::row4Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*3) );
    
    auto bubbleAction4 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*3) );
    runAction( bubbleAction4 );
}
void Bubble8::row5Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*4) );
    
    auto bubbleAction5 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*4) );
    runAction( bubbleAction5 );
}
void Bubble8::addEvents()
{
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        
        if(rect.containsPoint(p))
        {
            if(!isPopped)
                Bubble8::touchEvent(touch);
            return true; // to indicate that we have consumed it.
        }
        //CCLOG("dintcontainMySprite");
        return false; // we did not consume this event, pass thru.
    };
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}
void Bubble8::addGrapeEvent()
{
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        if(rect.containsPoint(p))
        {
            if(!isPopped){
                isClickTwo = false;
                nextTime = millisecondNow();
                if ((nextTime - preTime < 300) && (nextTime - preTime > 50)) {
                    scheduleOnce( schedule_selector(Bubble8::doublet), 0 );
                    isClickTwo = true;
                }
                scheduleOnce(schedule_selector(Bubble8::singlet), 0.25f);
                preTime = millisecondNow();
            }
            //Bubble6::touchRedEvent(touch);
            return true; // to indicate that we have consumed it.
        }
        return false; // we did not consume this event, pass thru.
    };
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}
void Bubble8::addGreenEvent()
{
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        if(rect.containsPoint(p))
        {
            if(!isPopped)
                Bubble8::touchGreenEvent(touch);
            return true; // to indicate that we have consumed it.
        }
        return false; // we did not consume this event, pass thru.
    };
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}
void Bubble8::addYellowEvent()
{
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        if(rect.containsPoint(p))
        {
            if(!isPopped)
                Bubble8::touchYellowEvent(touch);
            return true; // to indicate that we have consumed it.
        }
        return false; // we did not consume this event, pass thru.
    };
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}

void Bubble8::touchEvent(cocos2d::Touch* touch)
{
    isPopped = true;
    //removeChildByTag(int tag);
    setTexture("bubble_popped.png");
    GameWorld8::normal();
}

void Bubble8::touchRedEvent(cocos2d::Touch* touch)
{
    isPopped = true;
    setTexture("bubble_popped.png");
    GameWorld8::red();
}
void Bubble8::touchGreenEvent(cocos2d::Touch* touch)
{
    isPopped = true;
    setTexture("bubble_popped.png");
    GameWorld8::green();
}
void Bubble8::touchYellowEvent(cocos2d::Touch* touch)
{
    isPopped = true;
    setTexture("bubble_popped.png");
    GameWorld8::yellow();
}
void Bubble8::resumeBubble(float dt)
{
    //Director::getInstance()->resume();
}

void Bubble8::doublet(float dt)
{
    //this is Double click
    isPopped = true;
    setTexture("bubble_popped.png");
    cocos2d::Sprite* rowx =  cocos2d::Sprite::create( "bubble_strawberry.png" );
    auto emitter1 = ParticleSystemQuad::create("LavaFlow.plist");
    emitter1->setStartColor(Color4F(0.83,0.01,0.184,1));
    //emitter1->setStartColor(Color4F::MAGENTA);
    emitter1->setPosition(Point( rowx->getContentSize().width/2, rowx->getContentSize().height/2));
    emitter1->retain();
    addChild(emitter1, 100);
}
void Bubble8::singlet(float dt)
{
    //this is Single click
    if (! isClickTwo) {
        isPopped = true;
        setTexture("bubble_popped.png");
        GameWorld8::grape(getPositionX(),getPositionY());
    }
}




