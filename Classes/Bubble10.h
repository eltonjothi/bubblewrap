#ifndef __BUBBLE10_H__
#define __BUBBLE10_H__

#include "cocos2d.h"
#include "MyBodyParser.h"

#include "GameWorld10Scene.h"

//class Bubble
class Bubble10 : public cocos2d::Sprite
{
public:
    Bubble10( );
    ~Bubble10( );
    //cocos2d::Sprite *bubbleSprite;
    static Bubble10* create();
    static Bubble10* create2();
    static Bubble10* create3();
    static Bubble10* create4();
    static Bubble10* create5();
	cocos2d::Sprite *bubble;
	cocos2d::PhysicsBody *bubbleBody;
    
    bool isPopped = false;
    void row1Options();
    void row2Options();
    void row3Options();
    void row4Options();
    void row5Options();
    int offset;
    
    void addEvents();
    void addFruitEvent();
    void touchEvent(cocos2d::Touch* touch);
    void resumeBubble(float dt);
    
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event * event);

    long preTime;
    long nextTime;
    bool isClickTwo;
    long millisecondNow();
    
    void singlet(float dt);
    void doublet(float dt);

private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
};

#endif // __Bubble10_H__
