#ifndef __BUBBLE2_H__
#define __BUBBLE2_H__

#include "cocos2d.h"
#include "MyBodyParser.h"

#include "GameWorld2Scene.h"

//class Bubble
class Bubble2 : public cocos2d::Sprite
{
public:
    Bubble2( );
    ~Bubble2( );
    //cocos2d::Sprite *bubbleSprite;
    static Bubble2* create();
    static Bubble2* create2();
    static Bubble2* create3();
    static Bubble2* create4();
    static Bubble2* create5();
	cocos2d::Sprite *bubble;
	cocos2d::PhysicsBody *bubbleBody;
    
    bool isPopped = false;
    void row1Options();
    void row2Options();
    void row3Options();
    void row4Options();
    void row5Options();
    int offset;
    
    void addEvents();
    void addRedEvent();
    void touchEvent(cocos2d::Touch* touch);
    void touchRedEvent(cocos2d::Touch* touch);
    
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event * event);
    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
};

#endif // __BUBBLE2_H__
