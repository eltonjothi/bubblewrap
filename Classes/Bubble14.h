#ifndef __BUBBLE14_H__
#define __BUBBLE14_H__

#include "cocos2d.h"
#include "MyBodyParser.h"

#include "GameWorld14Scene.h"

//class Bubble
class Bubble14 : public cocos2d::Sprite
{
public:
    Bubble14( );
    ~Bubble14( );
    //cocos2d::Sprite *bubbleSprite;
    static Bubble14* create();
    static Bubble14* create2();
    static Bubble14* create3();
    static Bubble14* create4();
    static Bubble14* create5();
	cocos2d::Sprite *bubble;
	cocos2d::PhysicsBody *bubbleBody;
    
    bool isPopped = false;
    void row1Options();
    void row2Options();
    void row3Options();
    void row4Options();
    void row5Options();
    int offset;
    
    void addEvents();
    void addRedEvent();
    void addYellowEvent();
    void addGreenEvent();
    void touchEvent(cocos2d::Touch* touch);
    void touchRedEvent(cocos2d::Touch* touch);
    void touchGreenEvent(cocos2d::Touch* touch);
    void touchYellowEvent(cocos2d::Touch* touch);
    void resumeBubble(float dt);
    
    void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event * event);
    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
};

#endif // __Bubble14_H__
