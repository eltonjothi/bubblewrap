#ifndef __LEVELOVER_SCENE_H__
#define __LEVELOVER_SCENE_H__

#include "cocos2d.h"
#include "Parallax/CCParallaxScrollNode.h"
#include "Parallax/CCParallaxScrollOffset.h"
#include "PopupLayer.h"
#include "AnimSprite.h"
#include "GameWorldScene.h"
#include "GameWorld2Scene.h"
#include "GameWorld3Scene.h"
#include "GameWorld4Scene.h"
#include "GameWorld5Scene.h"
#include "GameWorld6Scene.h"
#include "GameWorld7Scene.h"
#include "GameWorld8Scene.h"
#include "GameWorld9Scene.h"
#include "GameWorld10Scene.h"
#include "GameWorld11Scene.h"
#include "GameWorld12Scene.h"
#include "GameWorld13Scene.h"
#include "GameWorld14Scene.h"
#include "GameWorld15Scene.h"

class LevelOver : public cocos2d::LayerColor
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene(unsigned int tempScore, unsigned int tempCurLvl, unsigned int type1, unsigned int type2, unsigned int type3, unsigned int type4, unsigned int type5, unsigned int type6, unsigned int type7, unsigned int type8, unsigned int type9, unsigned int type10);
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    int totalGameScore;
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);

    void onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event );
    bool isPopped;
    void quitPopUpLayer();
    void buttonCallback(cocos2d::Node *pNode);
    
    // implement the "static create()" method manually
    CREATE_FUNC(LevelOver);

    void GoToGameScene(Ref *sender);

};

#endif // __HELLOWORLD_SCENE_H__
