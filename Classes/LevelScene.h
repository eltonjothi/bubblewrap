#ifndef LEVELSCENE_H_
#define LEVELSCENE_H_

#include "cocos2d.h"
#include "cocos-ext.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class LevelScene : public cocos2d::LayerColor
{
public:
    
   // ScrollView *scrollView;
    Sprite *background;
    Layer *scrollContainer;
    
    virtual bool init();
    static cocos2d::Scene* createScene();
    
    void menuCallBack(CCObject *sender);
    void menuCloseCallback(cocos2d::Ref* pSender);
    //virtual void keyBackClicked();
    
    CREATE_FUNC(LevelScene);
   // LevelScene();
   // virtual LevelScene();
};

#endif /* LEVELSCENE_H_ */