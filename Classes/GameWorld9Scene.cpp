#include "GameWorld9Scene.h"

USING_NS_CC;


int timerCounter9;
bool isTimer9;
bool isTimerUpdate9;
int level9Score;
int level9Mango;

float posistionx9;
float posistiony9;
bool isTouched9;

Scene* GameWorld9::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics( );
    //scene->getPhysicsWorld( )->setDebugDrawMask( PhysicsWorld::DEBUGDRAW_ALL );
    
    // 'layer' is an autorelease object
    auto layer = GameWorld9::create();
	//SET GRAVITY
    scene->getPhysicsWorld()->setGravity(Vect(0, 0));

	layer->SetPhysicsWorld( scene->getPhysicsWorld( ) );
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameWorld9::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(0,0,0,0)) )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    Director::getInstance()->getScheduler()->setTimeScale(3.3);
    
    //LEVEL INTRO POP START
    auto backgroundSprite2 = Sprite::create( "pop.png" );
    backgroundSprite2->setPosition( Point(backgroundSprite2->getContentSize().width/2, backgroundSprite2->getContentSize().height/2) );
    
    isStarted=false;
    this->setTouchEnabled(false);
    //star1Sprite->setScale(2);
    
    LayerColor* _playerPopup = LayerColor::create(Color4B::BLACK);
    _playerPopup->setContentSize(CCSizeMake(visibleSize.width, backgroundSprite2->getContentSize().height));
    //_playerPopup->setPosition(Point(0, 0));
    _playerPopup->addChild(backgroundSprite2);
    auto LevelOverText = Label::createWithTTF("Pop as", "SuperMario256.ttf", visibleSize.height * 0.05 );
    LevelOverText->setColor(Color3B::WHITE);
    LevelOverText->setPosition(Point( _playerPopup->getContentSize().width/2, _playerPopup->getContentSize().height/2));
    _playerPopup->addChild(LevelOverText, SCORE_LAYER);
    
    this->addChild(_playerPopup,1000);
    
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        CCLOG("CONSUME");
        return true; // to indicate that we have consumed it.
    };
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, _playerPopup);
    
    auto callback = CallFunc::create( [this]() {
        // this->removeChild(_playerPopup);
        isStarted=true;
        
    });
    
    
    Action* popupLayer=Sequence::create(MoveTo::create(0.5, Point(0, 0)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2+_playerPopup->getContentSize().height/8)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2-_playerPopup->getContentSize().height/6)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2)),
                                        MoveTo::create(7, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2-_playerPopup->getContentSize().height/6)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height)),
                                        RemoveSelf::create(),
                                        callback,
                                        NULL);
    _playerPopup->runAction(popupLayer);
    
    //LEVEL INTRO POP ENDS

    
    screenPosition = -720;
    spawnNum = 0;

    //Set Resolution
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;

    auto backgroundSprite = Sprite::create( "bg1.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    this->addChild( backgroundSprite, 0);
    
    auto topPanelSprite = Sprite::create( "top_panel.png" );
    topPanelSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height - topPanelSprite->getContentSize().height/2) );
    this->addChild( topPanelSprite, 1);
    
    auto closeItem = MenuItemImage::create("quit.png",
                                           "quit.png",
                                           CC_CALLBACK_1(GameWorld9::menuCloseCallback, this));
    closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));
    // create menu, it's an autorelease object
    
    auto closeMenu = Menu::create(closeItem, NULL);
    closeMenu->setPosition(Vec2::ZERO);
    this->addChild(closeMenu, CLOSE_BUTTON_LAYER);
    
    isPopped = false;
    isPaused = false;
    isTimer9 = true;
    isTimerUpdate9 = true;
    
    auto image1 = MenuItemImage::create("sound_on_toggle.png", "sound_on_toggle.png", NULL, NULL );
    auto image2 = MenuItemImage::create("sound_off_toggle.png", "sound_off_toggle.png", NULL, NULL );
    
    auto toggle_item = MenuItemToggle::createWithCallback(CC_CALLBACK_1(GameWorld9::SoundCallback, this), image1 , image2, NULL);
    toggle_item->setPosition(Vec2(origin.x + visibleSize.width - toggle_item->getContentSize().width/2 ,
                                  origin.y + visibleSize.height - toggle_item->getContentSize().height/2));
    auto soundMenu = Menu::create( toggle_item, NULL );
    soundMenu->setPosition(Vec2::ZERO);
    this->addChild(soundMenu, 100);
    
    auto play = MenuItemImage::create("play_toggle.png", "play_toggle.png", NULL, NULL );
    auto pause = MenuItemImage::create("pause_toggle.png", "pause_toggle.png", NULL, NULL );
    pp_toggle_item = MenuItemToggle::createWithCallback(CC_CALLBACK_1(GameWorld9::pauseCallback, this), play , pause, NULL);;
    pp_toggle_item->setPosition(Vec2(origin.x + visibleSize.width - toggle_item->getContentSize().width*2 ,
                                     origin.y + visibleSize.height - toggle_item->getContentSize().height/2));
    auto ppMenu = Menu::create(pp_toggle_item, NULL);
    ppMenu->setPosition(Vec2::ZERO);
    this->addChild(ppMenu, 100);

    //MUTE
    def = UserDefault::getInstance( );
    isMuted = def->getBoolForKey("Mute");
    if(isMuted){
        CocosDenshion::SimpleAudioEngine::getInstance( )->setBackgroundMusicVolume(0);
        CocosDenshion::SimpleAudioEngine::getInstance( )->setEffectsVolume(0);
        toggle_item->setSelectedIndex(1);
    }
    
    //SCORE
    level9Score = 0;
    timerCounter9 =60;
    isTouched9 = false;
    level9Score = 0;
    timerCounter9 =60;
    level9Mango = 0;
    
    String *tempTimer = String::createWithFormat( "Timer - %i m", timerCounter9 );
    timerLabel = Label::createWithTTF( tempTimer->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_FONT_SIZE );
    timerLabel->setColor( Color3B::GRAY );
    timerLabel->enableOutline(Color4B::BLACK,2);
    timerLabel->setPosition( Point(  screenWidth-screenWidth/6, screenHeight - screenHeight/10) );
    this->addChild( timerLabel, 50);

    String *tempScore = String::createWithFormat( "Score - %i", level9Score );
    scoreLabel = Label::createWithTTF( tempScore->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_FONT_SIZE );
    scoreLabel->setColor( Color3B::ORANGE );
    scoreLabel->enableOutline(Color4B::BLACK,2);
    scoreLabel->setPosition( Point(  screenWidth/6, screenHeight - screenHeight/10) );
    this->addChild( scoreLabel, 22);
    
    this->schedule(schedule_selector(GameWorld9::SpawnEnemy),  0.5 ); //3
    this->schedule(schedule_selector(GameWorld9::UpdateTimer),1);
    this->scheduleOnce(schedule_selector(GameWorld9::normalSpeed), 10 );
    this->scheduleUpdate( );
    return true;
}
void GameWorld9::normalSpeed(float dt)
{
    Director::getInstance()->getScheduler()->setTimeScale(1);
}
void GameWorld9::UpdateTimer(float dt)
{
    if(isTimer9)
    timerCounter9--;
    
    if (timerCounter9 <= 0)
    {
        unschedule(schedule_selector(GameWorld9::SpawnEnemy));
        unschedule(schedule_selector(GameWorld9::UpdateTimer));
        this->scheduleOnce( schedule_selector( GameWorld9::NextLevel ), 0 );
    }
}
void GameWorld9::NextLevel( float dt )
{
    cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
    //auto scene = GameWorld10::createScene();
    auto scene = LevelOver::createScene(level9Score,9,0,0,0,0,0,0,0,0,0,0);
    cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}
void  GameWorld9::normal()
{
    level9Score++;
}
void  GameWorld9::mango(float Posx, float Posy)
{
    isTouched9 = true;
    posistionx9=Posx;
    posistiony9=Posy;
    level9Mango++;
    //timerCounter6 = timerCounter6-10;
}
void  GameWorld9::red()
{
    timerCounter9 = timerCounter9-10;
}
void  GameWorld9::green()
{
    timerCounter9 = timerCounter9+10;
}
void  GameWorld9::yellow()
{
    isTimer9 = false;
    isTimerUpdate9 = false;
}
void GameWorld9::yellowResume(float dt)
{
    isTimer9 = true;
}
void GameWorld9::SpawnEnemy(float dt)
{
    
    Bubble9* _3mySprite = Bubble9::create();
    this->addChild(_3mySprite,100);
    Bubble9* _3mySprite2 = Bubble9::create2();
    this->addChild(_3mySprite2,100);
    Bubble9* _3mySprite3 = Bubble9::create3();
    this->addChild(_3mySprite3,100);
    Bubble9* _3mySprite4 = Bubble9::create4();
    this->addChild(_3mySprite4,100);
    Bubble9* _2mySprite5 = Bubble9::create5();
    this->addChild(_2mySprite5,100);
}

void GameWorld9::update( float dt )
{
    
    __String *tempTimer = __String::createWithFormat( "Time - %i m", timerCounter9 );
    timerLabel->setString( tempTimer->getCString( ) );
    
    __String *tempScore = __String::createWithFormat( "Score - %i ", level9Score );
    scoreLabel->setString( tempScore->getCString( ) );
    
    if(!isTimerUpdate9){
        isTimerUpdate9 = true;
        this->scheduleOnce( schedule_selector( GameWorld9::yellowResume ), 5 );
    }

    if(isTouched9){
        ccBezierConfig bezier;
        bezier.controlPoint_1 = Vec2(-300, screenHeight-100);
        bezier.controlPoint_2 = Vec2(300, screenHeight-100);
        bezier.endPosition = Vec2(0,screenHeight);
        auto BubbleSprite = Sprite::create( "fu1.png" );
        BubbleSprite->setPosition( Point( posistionx9, posistiony9) );
        BubbleSprite->setScale(3);
        this->addChild(BubbleSprite,100);
        auto bubbleAction2 = BezierTo::create(1 , bezier );
        BubbleSprite->runAction( bubbleAction2 );
        isTouched9=false;
    }
}

void GameWorld9::pauseNodeAndDescendants(Node *pNode)
{
    pNode->pause();
    for(const auto &child : pNode->getChildren())
    {
        //if(pNode->getChildren() != this->ppMenu)
        this->pauseNodeAndDescendants(child);
    }
}
void GameWorld9::resumeNodeAndDescendants(Node *pNode)
{
    pNode->resume();
    for(const auto &child : pNode->getChildren())
    {
        this->resumeNodeAndDescendants(child);
    }
}
void GameWorld9::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
    MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif
    if(!isPopped){
    quitPopUpLayer();
    this->pause();
    pauseNodeAndDescendants(this);
    isPaused=true;
    }
}
void GameWorld9::pauseCallback(Ref* pSender)
{
    
    auto toggleItem = dynamic_cast<MenuItemToggle*>(pSender);
    if (toggleItem->getSelectedIndex() == 1 ) {
        this->pause();
        //this->pauseSchedulerAndActions();
        pauseNodeAndDescendants(this);
        isPaused=true;
    }else if ( toggleItem->getSelectedIndex() == 0 ) {
        this->resume();
        resumeNodeAndDescendants(this);
        isPaused=false;
    }
}

void GameWorld9::SoundCallback(Ref* pSender2)
{
    auto toggleItem = dynamic_cast<MenuItemToggle*>(pSender2);
    if (toggleItem->getSelectedIndex() == 1 ) {
        CocosDenshion::SimpleAudioEngine::getInstance( )->setBackgroundMusicVolume(0);
        CocosDenshion::SimpleAudioEngine::getInstance( )->setEffectsVolume(0);
        isMuted=true;
        //CCLOG("Muted");
    }
    
    else if ( toggleItem->getSelectedIndex() == 0 ) {
        CocosDenshion::SimpleAudioEngine::getInstance( )->setBackgroundMusicVolume(1);
        CocosDenshion::SimpleAudioEngine::getInstance( )->setEffectsVolume(1);
        isMuted=false;
        //CCLOG("Unmuted");
    }
    def->setBoolForKey("Mute", isMuted);
    def->flush( );
}

void GameWorld9::onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event )
{
    if(!isPopped)
    {
        quitPopUpLayer();
        this->pause();
        pauseNodeAndDescendants(this);
        isPaused=true;
    }
}
void GameWorld9::quitPopUpLayer(){
    isPopped = true;
    PopupLayer* pl = PopupLayer::create("pop_panel.png");
    pl->setContentSize(CCSizeMake(400, 150));
    pl->setTitle("");
    pl->setContentText("Do you want to quit playing ?", 20, 60, 250);
    pl->setCallbackFunc(this, callfuncN_selector(GameWorld9::buttonCallback));
    pl->addButton("pop_button.png", "pop_button.png", "No", 0);
    pl->addButton("pop_button.png", "pop_button.png", "Yes", 1);
    this->addChild(pl,20);
}
void GameWorld9::buttonCallback(cocos2d::Node *pNode){
    if(pNode->getTag() == 0)
    {
        isPopped = false;
        this->resume();
        resumeNodeAndDescendants(this);
    }else{
        Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        exit(0);
#endif
    }
}
