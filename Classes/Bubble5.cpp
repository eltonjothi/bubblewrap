#include "Bubble5.h"
#include "Definitions.h"

USING_NS_CC;

Bubble5::Bubble5(  ) {}

Bubble5::~Bubble5() {}

Bubble5* Bubble5::create()
{
    Bubble5* row1 = new Bubble5();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.80 )
    {
        row1->initWithFile("bubble_normal.png");
        row1->autorelease();
        row1->row1Options();
        row1->addEvents();
    }else if (randomBubble >= 0.80 && randomBubble < 0.90){
        row1->initWithFile("bubble_red_normal.png");
        row1->autorelease();
        row1->row1Options();
        row1->addRedEvent();
    }else if ( randomBubble >= 0.90 && randomBubble < 0.95){
        row1->initWithFile("bubble_green_normal.png");
        row1->autorelease();
        row1->row1Options();
        row1->addGreenEvent();
    }else if ( randomBubble >= 0.95){
        row1->initWithFile("bubble_yellow_normal.png");
        row1->autorelease();
        row1->row1Options();
        row1->addYellowEvent();
    }
    return row1;
    CC_SAFE_DELETE(row1);
    return NULL;
}
Bubble5* Bubble5::create2()
{
    Bubble5* row2 = new Bubble5();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.80 )
    {
        row2->initWithFile("bubble_normal.png");
        row2->autorelease();
        row2->row2Options();
        row2->addEvents();
    }else if (randomBubble >= 0.80 && randomBubble < 0.90){
        row2->initWithFile("bubble_red_normal.png");
        row2->autorelease();
        row2->row2Options();
        row2->addRedEvent();
    }else if ( randomBubble >= 0.90 && randomBubble < 0.95){
        row2->initWithFile("bubble_green_normal.png");
        row2->autorelease();
        row2->row2Options();
        row2->addGreenEvent();
    }else if ( randomBubble >= 0.95){
        row2->initWithFile("bubble_yellow_normal.png");
        row2->autorelease();
        row2->row2Options();
        row2->addYellowEvent();
    }
    return row2;
    CC_SAFE_DELETE(row2);
    return NULL;
}
Bubble5* Bubble5::create3()
{
    Bubble5* row3 = new Bubble5();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.80 )
    {
        row3->initWithFile("bubble_normal.png");
        row3->autorelease();
        row3->row3Options();
        row3->addEvents();
    }else if (randomBubble >= 0.80 && randomBubble < 0.90){
        row3->initWithFile("bubble_red_normal.png");
        row3->autorelease();
        row3->row3Options();
        row3->addRedEvent();
    }else if ( randomBubble >= 0.90 && randomBubble < 0.95){
        row3->initWithFile("bubble_green_normal.png");
        row3->autorelease();
        row3->row3Options();
        row3->addGreenEvent();
    }else if ( randomBubble >= 0.95){
        row3->initWithFile("bubble_yellow_normal.png");
        row3->autorelease();
        row3->row3Options();
        row3->addYellowEvent();
    }
    return row3;
    CC_SAFE_DELETE(row3);
    return NULL;
}
Bubble5* Bubble5::create4()
{
    Bubble5* row4 = new Bubble5();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.80 )
    {
        row4->initWithFile("bubble_normal.png");
        row4->autorelease();
        row4->row4Options();
        row4->addEvents();
    }else if (randomBubble >= 0.80 && randomBubble < 0.90){
        row4->initWithFile("bubble_red_normal.png");
        row4->autorelease();
        row4->row4Options();
        row4->addRedEvent();
    }else if ( randomBubble >= 0.90 && randomBubble < 0.95){
        row4->initWithFile("bubble_green_normal.png");
        row4->autorelease();
        row4->row4Options();
        row4->addGreenEvent();
    }else if ( randomBubble >= 0.95){
        row4->initWithFile("bubble_yellow_normal.png");
        row4->autorelease();
        row4->row4Options();
        row4->addYellowEvent();
    }
    return row4;
    CC_SAFE_DELETE(row4);
    return NULL;
}
Bubble5* Bubble5::create5()
{
    Bubble5* row5 = new Bubble5();
    auto randomBubble = CCRANDOM_0_1( );
    if ( randomBubble < 0.80 )
    {
        row5->initWithFile("bubble_normal.png");
        row5->autorelease();
        row5->row5Options();
        row5->addEvents();
    }else if (randomBubble >= 0.80 && randomBubble < 0.90){
        row5->initWithFile("bubble_red_normal.png");
        row5->autorelease();
        row5->row5Options();
        row5->addRedEvent();
    }else if ( randomBubble >= 0.90 && randomBubble < 0.95){
        row5->initWithFile("bubble_green_normal.png");
        row5->autorelease();
        row5->row5Options();
        row5->addGreenEvent();
    }else if ( randomBubble >= 0.95){
        row5->initWithFile("bubble_yellow_normal.png");
        row5->autorelease();
        row5->row5Options();
        row5->addYellowEvent();
    }
    return row5;
    CC_SAFE_DELETE(row5);
    return NULL;
}
void Bubble5::row1Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4) );
    
    auto bubbleAction = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4) );
    runAction( bubbleAction );
}
void Bubble5::row2Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset) );
    
    auto bubbleAction2 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset) );
    runAction( bubbleAction2 );
}
void Bubble5::row3Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*2) );
    
    auto bubbleAction3 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*2) );
    runAction( bubbleAction3 );
}
void Bubble5::row4Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*3) );
    
    auto bubbleAction4 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*3) );
    runAction( bubbleAction4 );
}
void Bubble5::row5Options()
{
    isPopped = false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    setScale(3);
    offset = visibleSize.height/BUBBLE_OFFSET;
    setPosition( Point( -visibleSize.width/2 , visibleSize.height/2 + visibleSize.height/4 - offset*4) );
    
    auto bubbleAction5 = MoveTo::create( 12  , Point(visibleSize.width+visibleSize.width/2,  visibleSize.height/2+ visibleSize.height/4 - offset*4) );
    runAction( bubbleAction5 );
}
void Bubble5::addEvents()
{
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        
        if(rect.containsPoint(p))
        {
            if(!isPopped)
                Bubble5::touchEvent(touch);
            return true; // to indicate that we have consumed it.
        }
        //CCLOG("dintcontainMySprite");
        return false; // we did not consume this event, pass thru.
    };
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}
void Bubble5::addRedEvent()
{
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        if(rect.containsPoint(p))
        {
            if(!isPopped)
                Bubble5::touchRedEvent(touch);
            return true; // to indicate that we have consumed it.
        }
        return false; // we did not consume this event, pass thru.
    };
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}
void Bubble5::addGreenEvent()
{
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        if(rect.containsPoint(p))
        {
            if(!isPopped)
                Bubble5::touchGreenEvent(touch);
            return true; // to indicate that we have consumed it.
        }
        return false; // we did not consume this event, pass thru.
    };
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}
void Bubble5::addYellowEvent()
{
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        cocos2d::Vec2 p = touch->getLocation();
        cocos2d::Rect rect = this->getBoundingBox();
        if(rect.containsPoint(p))
        {
            if(!isPopped)
                Bubble5::touchYellowEvent(touch);
            return true; // to indicate that we have consumed it.
        }
        return false; // we did not consume this event, pass thru.
    };
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}

void Bubble5::touchEvent(cocos2d::Touch* touch)
{
    isPopped = true;
    //removeChildByTag(int tag);
    setTexture("bubble_popped.png");
    GameWorld5::normal();
}

void Bubble5::touchRedEvent(cocos2d::Touch* touch)
{
    isPopped = true;
    setTexture("bubble_popped.png");
    cocos2d::Sprite* rowx =  cocos2d::Sprite::create( "bubble_strawberry.png" );
    auto emitter1 = ParticleSystemQuad::create("LavaFlow.plist");
    emitter1->setStartColor(Color4F(0.83,0.01,0.184,1));
    //emitter1->setStartColor(Color4F::MAGENTA);
    emitter1->setPosition(Point( rowx->getContentSize().width/2, rowx->getContentSize().height/2));
    emitter1->retain();
    addChild(emitter1, 100);
    GameWorld5::red();
}
void Bubble5::touchGreenEvent(cocos2d::Touch* touch)
{
    isPopped = true;
    setTexture("bubble_popped.png");
    cocos2d::Sprite* rowx =  cocos2d::Sprite::create( "bubble_strawberry.png" );
    auto emitter1 = ParticleSystemQuad::create("LavaFlow.plist");
    emitter1->setStartColor(Color4F(0.83,0.01,0.184,1));
    //emitter1->setStartColor(Color4F::MAGENTA);
    emitter1->setPosition(Point( rowx->getContentSize().width/2, rowx->getContentSize().height/2));
    emitter1->retain();
    addChild(emitter1, 100);
    GameWorld5::green();
}
void Bubble5::touchYellowEvent(cocos2d::Touch* touch)
{
    isPopped = true;
    setTexture("bubble_popped.png");
    cocos2d::Sprite* rowx =  cocos2d::Sprite::create( "bubble_strawberry.png" );
    auto emitter1 = ParticleSystemQuad::create("LavaFlow.plist");
    emitter1->setStartColor(Color4F(0.83,0.01,0.184,1));
    //emitter1->setStartColor(Color4F::MAGENTA);
    emitter1->setPosition(Point( rowx->getContentSize().width/2, rowx->getContentSize().height/2));
    emitter1->retain();
    addChild(emitter1, 100);
    GameWorld5::yellow();
}
void Bubble5::resumeBubble(float dt)
{
    //Director::getInstance()->resume();
}




