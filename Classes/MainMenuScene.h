#ifndef __MAINMENU_SCENE_H__
#define __MAINMENU_SCENE_H__

#include "cocos2d.h"
#include "HelpScene.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AdmobHelper.h"
#endif

class MainMenu : public cocos2d::LayerColor
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    float screenWidth;
    float screenHeight;
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    // a selector callback
    void onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event );
    void menuCloseCallback(cocos2d::Ref* pSender);
    void fbCallback(Ref* pSender);
    void GoToGameScene(Ref *sender);
    void GoToHelpScene(Ref *sender);

    bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event * event);
    
    bool isPopped;
    void quitPopUpLayer();
    void helpPopUpLayer();
    void buttonCallback(cocos2d::Node *pNode);
    // implement the "static create()" method manually
    CREATE_FUNC(MainMenu);
};

#endif // __HELLOWORLD_SCENE_H__
