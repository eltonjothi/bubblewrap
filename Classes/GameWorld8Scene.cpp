#include "GameWorld8Scene.h"

USING_NS_CC;


int timerCounter8;
bool isTimer8;
bool isTimerUpdate8;
int level8Score;
int level8Grape;

float posistionx8;
float posistiony8;
bool isTouched8;

Scene* GameWorld8::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics( );
    //scene->getPhysicsWorld( )->setDebugDrawMask( PhysicsWorld::DEBUGDRAW_ALL );
    
    // 'layer' is an autorelease object
    auto layer = GameWorld8::create();
	//SET GRAVITY
    scene->getPhysicsWorld()->setGravity(Vect(0, 0));

	layer->SetPhysicsWorld( scene->getPhysicsWorld( ) );
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameWorld8::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(0,0,0,0)) )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    Director::getInstance()->getScheduler()->setTimeScale(3.3);
    
    //LEVEL INTRO POP START
    auto backgroundSprite2 = Sprite::create( "pop.png" );
    backgroundSprite2->setPosition( Point(backgroundSprite2->getContentSize().width/2, backgroundSprite2->getContentSize().height/2) );
    
    isStarted=false;
    this->setTouchEnabled(false);
    //star1Sprite->setScale(2);
    
    LayerColor* _playerPopup = LayerColor::create(Color4B::BLACK);
    _playerPopup->setContentSize(CCSizeMake(visibleSize.width, backgroundSprite2->getContentSize().height));
    //_playerPopup->setPosition(Point(0, 0));
    _playerPopup->addChild(backgroundSprite2);
    auto LevelOverText = Label::createWithTTF("Pop as", "SuperMario256.ttf", visibleSize.height * 0.05 );
    LevelOverText->setColor(Color3B::WHITE);
    LevelOverText->setPosition(Point( _playerPopup->getContentSize().width/2, _playerPopup->getContentSize().height/2));
    _playerPopup->addChild(LevelOverText, SCORE_LAYER);
    
    this->addChild(_playerPopup,1000);
    
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        CCLOG("CONSUME");
        return true; // to indicate that we have consumed it.
    };
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, _playerPopup);
    
    auto callback = CallFunc::create( [this]() {
        // this->removeChild(_playerPopup);
        isStarted=true;
        
    });
    
    
    Action* popupLayer=Sequence::create(MoveTo::create(0.5, Point(0, 0)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2+_playerPopup->getContentSize().height/8)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2-_playerPopup->getContentSize().height/6)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2)),
                                        MoveTo::create(7, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height/2-_playerPopup->getContentSize().height/2-_playerPopup->getContentSize().height/6)),
                                        MoveTo::create(0.5, Point(0, visibleSize.height)),
                                        RemoveSelf::create(),
                                        callback,
                                        NULL);
    _playerPopup->runAction(popupLayer);
    
    //LEVEL INTRO POP ENDS

    
    screenPosition = -720;
    spawnNum = 0;

    //Set Resolution
    screenWidth = visibleSize.width;
    screenHeight = visibleSize.height;

    auto backgroundSprite = Sprite::create( "bg1.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    this->addChild( backgroundSprite, 0);
    
    auto topPanelSprite = Sprite::create( "top_panel.png" );
    topPanelSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height - topPanelSprite->getContentSize().height/2) );
    this->addChild( topPanelSprite, 1);
    
    auto closeItem = MenuItemImage::create("quit.png",
                                           "quit.png",
                                           CC_CALLBACK_1(GameWorld8::menuCloseCallback, this));
    closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));
    // create menu, it's an autorelease object
    
    auto closeMenu = Menu::create(closeItem, NULL);
    closeMenu->setPosition(Vec2::ZERO);
    this->addChild(closeMenu, CLOSE_BUTTON_LAYER);
    
    isPopped = false;
    isPaused = false;
    isTimer8 = true;
    isTimerUpdate8 = true;
    
    auto image1 = MenuItemImage::create("sound_on_toggle.png", "sound_on_toggle.png", NULL, NULL );
    auto image2 = MenuItemImage::create("sound_off_toggle.png", "sound_off_toggle.png", NULL, NULL );
    
    auto toggle_item = MenuItemToggle::createWithCallback(CC_CALLBACK_1(GameWorld8::SoundCallback, this), image1 , image2, NULL);
    toggle_item->setPosition(Vec2(origin.x + visibleSize.width - toggle_item->getContentSize().width/2 ,
                                  origin.y + visibleSize.height - toggle_item->getContentSize().height/2));
    auto soundMenu = Menu::create( toggle_item, NULL );
    soundMenu->setPosition(Vec2::ZERO);
    this->addChild(soundMenu, 100);
    
    auto play = MenuItemImage::create("play_toggle.png", "play_toggle.png", NULL, NULL );
    auto pause = MenuItemImage::create("pause_toggle.png", "pause_toggle.png", NULL, NULL );
    pp_toggle_item = MenuItemToggle::createWithCallback(CC_CALLBACK_1(GameWorld8::pauseCallback, this), play , pause, NULL);;
    pp_toggle_item->setPosition(Vec2(origin.x + visibleSize.width - toggle_item->getContentSize().width*2 ,
                                     origin.y + visibleSize.height - toggle_item->getContentSize().height/2));
    auto ppMenu = Menu::create(pp_toggle_item, NULL);
    ppMenu->setPosition(Vec2::ZERO);
    this->addChild(ppMenu, 100);

    //MUTE
    def = UserDefault::getInstance( );
    isMuted = def->getBoolForKey("Mute");
    if(isMuted){
        CocosDenshion::SimpleAudioEngine::getInstance( )->setBackgroundMusicVolume(0);
        CocosDenshion::SimpleAudioEngine::getInstance( )->setEffectsVolume(0);
        toggle_item->setSelectedIndex(1);
    }
    
    //SCORE
    level8Score = 0;
    timerCounter8 =60;
    isTouched8 = false;
    level8Score = 0;
    timerCounter8 =60;
    level8Grape = 0;
    
    String *tempTimer = String::createWithFormat( "Timer - %i m", timerCounter8 );
    timerLabel = Label::createWithTTF( tempTimer->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_FONT_SIZE );
    timerLabel->setColor( Color3B::GRAY );
    timerLabel->enableOutline(Color4B::BLACK,2);
    timerLabel->setPosition( Point(  screenWidth-screenWidth/6, screenHeight - screenHeight/10) );
    this->addChild( timerLabel, 50);

    String *tempScore = String::createWithFormat( "Score - %i", level8Score );
    scoreLabel = Label::createWithTTF( tempScore->getCString( ), "SuperMario256.ttf", visibleSize.height * SCORE_FONT_SIZE );
    scoreLabel->setColor( Color3B::ORANGE );
    scoreLabel->enableOutline(Color4B::BLACK,2);
    scoreLabel->setPosition( Point(  screenWidth/6, screenHeight - screenHeight/10) );
    this->addChild( scoreLabel, 22);
    
    this->schedule(schedule_selector(GameWorld8::SpawnEnemy),  0.5 ); //3
    this->schedule(schedule_selector(GameWorld8::UpdateTimer),1);
    this->scheduleOnce(schedule_selector(GameWorld8::normalSpeed), 10 );
    this->scheduleUpdate( );
    return true;
}
void GameWorld8::normalSpeed(float dt)
{
    Director::getInstance()->getScheduler()->setTimeScale(1);
}
void GameWorld8::UpdateTimer(float dt)
{
    if(isTimer8)
    timerCounter8--;
    
    if (timerCounter8 <= 0)
    {
        unschedule(schedule_selector(GameWorld8::SpawnEnemy));
        unschedule(schedule_selector(GameWorld8::UpdateTimer));
        this->scheduleOnce( schedule_selector( GameWorld8::NextLevel ), 0 );
    }
}
void GameWorld8::NextLevel( float dt )
{
    cocos2d::Director::getInstance()->getEventDispatcher()->removeAllEventListeners();
    //auto scene = GameWorld9::createScene();
    auto scene = LevelOver::createScene(level8Score,8,0,0,0,0,0,0,0,0,0,0);
    cocos2d::Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}
void  GameWorld8::normal()
{
    level8Score++;
}
void  GameWorld8::grape(float Posx, float Posy)
{
    isTouched8 = true;
    posistionx8=Posx;
    posistiony8=Posy;
    level8Grape++;
    //timerCounter6 = timerCounter6-10;
}
void  GameWorld8::red()
{
    timerCounter8 = timerCounter8-10;
}
void  GameWorld8::green()
{
    timerCounter8 = timerCounter8+10;
}
void  GameWorld8::yellow()
{
    isTimer8 = false;
    isTimerUpdate8 = false;
}
void GameWorld8::yellowResume(float dt)
{
    isTimer8 = true;
}
void GameWorld8::SpawnEnemy(float dt)
{
    
    Bubble8* _3mySprite = Bubble8::create();
    this->addChild(_3mySprite,100);
    Bubble8* _3mySprite2 = Bubble8::create2();
    this->addChild(_3mySprite2,100);
    Bubble8* _3mySprite3 = Bubble8::create3();
    this->addChild(_3mySprite3,100);
    Bubble8* _3mySprite4 = Bubble8::create4();
    this->addChild(_3mySprite4,100);
    Bubble8* _2mySprite5 = Bubble8::create5();
    this->addChild(_2mySprite5,100);
}

void GameWorld8::update( float dt )
{
    
    __String *tempTimer = __String::createWithFormat( "Time - %i m", timerCounter8 );
    timerLabel->setString( tempTimer->getCString( ) );
    
    __String *tempScore = __String::createWithFormat( "Score - %i ", level8Score );
    scoreLabel->setString( tempScore->getCString( ) );
    
    if(!isTimerUpdate8){
        isTimerUpdate8 = true;
        this->scheduleOnce( schedule_selector( GameWorld8::yellowResume ), 5 );
    }
    
    if(isTouched8){
        ccBezierConfig bezier;
        bezier.controlPoint_1 = Vec2(-300, screenHeight-100);
        bezier.controlPoint_2 = Vec2(300, screenHeight-100);
        bezier.endPosition = Vec2(0,screenHeight);
        auto BubbleSprite = Sprite::create( "fu1.png" );
        BubbleSprite->setPosition( Point( posistionx8, posistiony8) );
        BubbleSprite->setScale(3);
        this->addChild(BubbleSprite,100);
        auto bubbleAction2 = BezierTo::create(1 , bezier );
        BubbleSprite->runAction( bubbleAction2 );
        isTouched8=false;
    }
    
}

void GameWorld8::pauseNodeAndDescendants(Node *pNode)
{
    pNode->pause();
    for(const auto &child : pNode->getChildren())
    {
        //if(pNode->getChildren() != this->ppMenu)
        this->pauseNodeAndDescendants(child);
    }
}
void GameWorld8::resumeNodeAndDescendants(Node *pNode)
{
    pNode->resume();
    for(const auto &child : pNode->getChildren())
    {
        this->resumeNodeAndDescendants(child);
    }
}
void GameWorld8::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
    MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif
    if(!isPopped){
    quitPopUpLayer();
    this->pause();
    pauseNodeAndDescendants(this);
    isPaused=true;
    }
}
void GameWorld8::pauseCallback(Ref* pSender)
{
    
    auto toggleItem = dynamic_cast<MenuItemToggle*>(pSender);
    if (toggleItem->getSelectedIndex() == 1 ) {
        this->pause();
        //this->pauseSchedulerAndActions();
        pauseNodeAndDescendants(this);
        isPaused=true;
    }else if ( toggleItem->getSelectedIndex() == 0 ) {
        this->resume();
        resumeNodeAndDescendants(this);
        isPaused=false;
    }
}

void GameWorld8::SoundCallback(Ref* pSender2)
{
    auto toggleItem = dynamic_cast<MenuItemToggle*>(pSender2);
    if (toggleItem->getSelectedIndex() == 1 ) {
        CocosDenshion::SimpleAudioEngine::getInstance( )->setBackgroundMusicVolume(0);
        CocosDenshion::SimpleAudioEngine::getInstance( )->setEffectsVolume(0);
        isMuted=true;
        //CCLOG("Muted");
    }
    
    else if ( toggleItem->getSelectedIndex() == 0 ) {
        CocosDenshion::SimpleAudioEngine::getInstance( )->setBackgroundMusicVolume(1);
        CocosDenshion::SimpleAudioEngine::getInstance( )->setEffectsVolume(1);
        isMuted=false;
        //CCLOG("Unmuted");
    }
    def->setBoolForKey("Mute", isMuted);
    def->flush( );
}

void GameWorld8::onKeyReleased( cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event )
{
    if(!isPopped)
    {
        quitPopUpLayer();
        this->pause();
        pauseNodeAndDescendants(this);
        isPaused=true;
    }
}
void GameWorld8::quitPopUpLayer(){
    isPopped = true;
    PopupLayer* pl = PopupLayer::create("pop_panel.png");
    pl->setContentSize(CCSizeMake(400, 150));
    pl->setTitle("");
    pl->setContentText("Do you want to quit playing ?", 20, 60, 250);
    pl->setCallbackFunc(this, callfuncN_selector(GameWorld8::buttonCallback));
    pl->addButton("pop_button.png", "pop_button.png", "No", 0);
    pl->addButton("pop_button.png", "pop_button.png", "Yes", 1);
    this->addChild(pl,20);
}
void GameWorld8::buttonCallback(cocos2d::Node *pNode){
    if(pNode->getTag() == 0)
    {
        isPopped = false;
        this->resume();
        resumeNodeAndDescendants(this);
    }else{
        Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        exit(0);
#endif
    }
}
