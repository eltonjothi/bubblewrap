#include "HelloWorldScene.h"
#include "GameWorldScene.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
    // Create the scrollview by vertical
    ui::ScrollView* scrollView = ui::ScrollView::create();
    scrollView->setContentSize(Size(visibleSize.width, visibleSize.height));
    //Size backgroundSize = background->getContentSize();
    scrollView->setPosition(Vec2((visibleSize.width - scrollView->getContentSize().width) / 2.0f,
                                 (visibleSize.height - scrollView->getContentSize().height) / 2.0f));
    this->addChild(scrollView);
    
    ImageView* imageView = ImageView::create("ccicon.png");
    
    float innerWidth = scrollView->getContentSize().width;
    float innerHeight = scrollView->getContentSize().height + imageView->getContentSize().height;
    
    scrollView->setInnerContainerSize(Size(innerWidth, innerHeight));
    
    Button* button = Button::create("animationbuttonnormal.png", "animationbuttonpressed.png");
    button->setPosition(Vec2(innerWidth / 2.0f, scrollView->getInnerContainerSize().height - button->getContentSize().height / 2.0f));
    scrollView->addChild(button);
    
    Button* titleButton = Button::create("backtotopnormal.png", "backtotoppressed.png");
    titleButton->setTitleText("Title Button");
    titleButton->setPosition(Vec2(innerWidth / 2.0f, button->getBottomBoundary() - button->getContentSize().height));
    scrollView->addChild(titleButton);
    
    Button* button_scale9 = Button::create("button.png", "buttonHighlighted.png");
    button_scale9->setScale9Enabled(true);
    button_scale9->setContentSize(Size(100.0f, button_scale9->getVirtualRendererSize().height));
    button_scale9->setPosition(Vec2(innerWidth / 2.0f, titleButton->getBottomBoundary() - titleButton->getContentSize().height));
    scrollView->addChild(button_scale9);
    
    imageView->setPosition(Vec2(innerWidth / 2.0f, imageView->getContentSize().height / 2.0f));
    scrollView->addChild(imageView);
    
    return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
