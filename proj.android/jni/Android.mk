LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos/ui)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/GameWorldScene.cpp \
                   ../../Classes/GameWorld2Scene.cpp \
                   ../../Classes/GameWorld3Scene.cpp \
                   ../../Classes/GameWorld4Scene.cpp \
                   ../../Classes/GameWorld5Scene.cpp \
                   ../../Classes/GameWorld6Scene.cpp \
                   ../../Classes/GameWorld7Scene.cpp \
                   ../../Classes/GameWorld8Scene.cpp \
                   ../../Classes/GameWorld9Scene.cpp \
                   ../../Classes/GameWorld10Scene.cpp \
                   ../../Classes/GameWorld11Scene.cpp \
                   ../../Classes/GameWorld12Scene.cpp \
                   ../../Classes/GameWorld13Scene.cpp \
                   ../../Classes/GameWorld14Scene.cpp \
                   ../../Classes/GameWorld15Scene.cpp \
                   ../../Classes/GameOverScene.cpp \
                   ../../Classes/MainMenuScene.cpp \
                   ../../Classes/AdmobHelper.cpp \
                   ../../Classes/Bubble.cpp \
                   ../../Classes/Bubble2.cpp \
                   ../../Classes/Bubble3.cpp \
                   ../../Classes/Bubble4.cpp \
                   ../../Classes/Bubble5.cpp \
                   ../../Classes/Bubble6.cpp \
                   ../../Classes/Bubble7.cpp \
                   ../../Classes/Bubble8.cpp \
                   ../../Classes/Bubble9.cpp \
                   ../../Classes/Bubble10.cpp \
                   ../../Classes/Bubble11.cpp \
                   ../../Classes/Bubble12.cpp \
                   ../../Classes/Bubble13.cpp \
                   ../../Classes/Bubble14.cpp \
                   ../../Classes/Bubble15.cpp \
                   ../../Classes/HelpScene.cpp \
                   ../../Classes/SplashScreenScene.cpp \
                   ../../Classes/MyBodyParser.cpp \
                   ../../Classes/Parallax/CCParallaxScrollNode.cpp \
                   ../../Classes/PopupLayer.cpp \
                   ../../Classes/Parallax/CCParallaxScrollOffset.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static

# LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
# LOCAL_WHOLE_STATIC_LIBRARIES += cocosbuilder_static
# LOCAL_WHOLE_STATIC_LIBRARIES += spine_static
# LOCAL_WHOLE_STATIC_LIBRARIES += cocostudio_static
# LOCAL_WHOLE_STATIC_LIBRARIES += cocos_network_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_ui_static


include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
$(call import-module,audio/android)

# $(call import-module,Box2D)
# $(call import-module,editor-support/cocosbuilder)
# $(call import-module,editor-support/spine)
# $(call import-module,editor-support/cocostudio)
# $(call import-module,network)
$(call import-module,extensions)
$(call import-module,ui)
